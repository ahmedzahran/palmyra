<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAirportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_en' => 'required|string',
            'name_ar' => 'required|string',
            'municipality_en' => 'required|string',
            'municipality_ar' => 'required|string',
            'country_id' => 'required|exists:countries,id',

            'IATA_code' => 'required|unique:airports,IATA_code,'.$this->segment(4)
        ];
    }
}
