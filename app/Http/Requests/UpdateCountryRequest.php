<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCountryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_en' => 'required|string',
            'name_ar' => 'required|string',
            'iso_code' => 'required|unique:countries,iso_code,'.$this->segment(4),
            'country_code' => 'required|string',
            'currency_id' => 'nullable|exists:currencies,id',
            'status_id' => 'required|numeric'
        ];
    }
}
