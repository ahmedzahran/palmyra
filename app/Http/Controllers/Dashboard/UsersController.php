<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Yajra\DataTables\Facades\DataTables;

class UsersController extends Controller
{
    
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $users = User::orderBy('created_at', 'desc')->select('*');

            $datatables = DataTables::of($users)
            ->addIndexColumn()
            ->addColumn('actions',function($q){
                
                $actions = '';
                if(auth()->id() != $q->id && auth()->user()->isAbleTo('user-delete')){
                    $delete = view('backend.datatable.action-delete')
                                ->with(['route' => route('admin.users.delete',['user_id' => $q->id,'allow_delete' => true])])
                                ->render();
                                
                    $actions .= $delete;
                }

                if(auth()->user()->isAbleTo('user-edit')){
                    $edit = view('backend.datatable.action-edit')
                        ->with(['route' => route('admin.users.edit', ['user_id' => $q->id])])
                        ->render();
                    $actions .= $edit;
                }
                
                return $actions;

            });

            if (request('name') != '') {
                $users->where('name','LIKE','%'.request('name').'%');
            }

            if (request('email') != '') {
                $users->where('email',request('email'));
            }

            if (request('phone') != '') {
                $users->where('phone',request('phone'));
            }

            if (request('provider_id') != '') {
                $users->where('provider',request('provider_id'));
            }

            if (request('from') != '') {
                $users->whereDate('created_at', '>=', request('from'));
            }

            if (request('to') != '') {
                $users->whereDate('created_at', '<=', request('to'));
            }
            return $datatables->rawColumns(['actions'])->make(true);
        }
        return view('backend.users.index');
    }

    public function delete($id)
    {
        if(auth()->id() == $id){
            abort('404');
        }

        $user = User::findOrFail($id);
        $user->syncRoles([]);
        $user->syncPermissions([]);
        $user->delete();
        return back()->withFlashSuccess(trans('alerts.backend.general.created'));
    }

    public function edit($user_id)
    {
        $user = User::findOrFail($user_id);
        $roles = Role::all();
        $permissions = Permission::all();

        return view('backend.users.edit',compact('user','roles','permissions'));
    }

    public function update(UpdateUserRequest $request,$id)
    {

        $user = User::findOrFail($id);

        $user->update(request()->except(['role_id','perimissions']));

        $user->syncRoles([request('role_id')]);

        $user->syncPermissions(request('perimissions'));

        return redirect()->back()->withFlashSuccess(trans('alerts.backend.general.updated'));

    }

    public function create()
    {
        $roles = Role::all();
        $permissions = Permission::all();

        return view('backend.users.create',compact('roles','permissions'));
    }

    public function store(StoreUserRequest $request)
    {
        $user = User::create([
            'name' => request('name'),
            'email' => request('email'),
            'phone' => request('phone'),
            'password' => bcrypt(request('password'))
        ]);

        $user->attachRole(request('role_id'));
        
        if(request()->has('perimissions')){
            $user->syncPermissions(request('perimissions'));
        }

        return redirect()->route('admin.users.index')->withFlashSuccess(trans('alerts.backend.general.created'));

    }
}
