<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class DashboardController extends Controller
{
    public function index()
    {
        return view('backend.dashboard');
    }

    public function cahngeLang($lang)
    {
        Session::put('locale', $lang);

        return back();
    }
}
