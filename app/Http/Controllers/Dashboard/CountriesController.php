<?php

namespace App\Http\Controllers\Dashboard;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCountryRequest;
use App\Http\Requests\UpdateCountryRequest;
use App\Models\Country;
use App\Models\Currency;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class CountriesController extends Controller
{
    
    public function index(Request $request)
    {

        if($request->ajax()){
            $countries = Country::orderBy('created_at', 'desc')->select('*');

            $datatables = DataTables::of($countries)
            ->addIndexColumn()
            ->editColumn('name',function($q){
                return $q->name;
            })
            ->editColumn('status',function($q){
                if ($q->status == 1){
                    return '<span class="label label-lg font-weight-bold label-light-primary label-inline">'.trans('labels.backend.general.active').'</span>';
                }

                return '<span class="label label-lg font-weight-bold label-light-danger label-inline">'.trans('labels.backend.general.unactive').'</span>';

            })
            ->addColumn('actions',function($q){
                
                $actions = '';
                if(auth()->user()->isAbleTo('country-delete')){
                    $delete = view('backend.datatable.action-delete')
                                ->with(['route' => route('admin.countries.delete',['country_id' => $q->id,'allow_delete' => true])])
                                ->render();
                                
                    $actions .= $delete;
                }

                if(auth()->user()->isAbleTo('country-edit')){
                    $edit = view('backend.datatable.action-edit')
                        ->with(['route' => route('admin.countries.edit', ['country_id' => $q->id])])
                        ->render();
                    $actions .= $edit;
                }
                
                return $actions;

            });

            if (request('name') != '') {

                $countries->where(DB::raw('lower(name->"$.en")'), "LIKE", "%".strtolower(request('name'))."%")
                        ->orWhere(DB::raw('lower(name->"$.ar")'), "LIKE", "%".strtolower(request('name'))."%");

            }

            if (request('country_code') != '') {
                $countries->where('country_code',request('country_code'));
            }

            if (request('status_id') != '') {
                $countries->where('status',request('status_id'));
            }

            if (request('iso_code') != '') {
                $countries->where('iso_code',request('iso_code'));
            }

            if (request('from') != '') {
                $countries->whereDate('created_at', '>=', request('from'));
            }

            if (request('to') != '') {
                $countries->whereDate('created_at', '<=', request('to'));
            }
            return $datatables->rawColumns(['actions','status'])->make(true);
        }

        return view('backend.countries.index');
    }


    public function create()
    {
        $currencies = Currency::all();
        return view('backend.countries.create',compact('currencies'));
    }

    public function store(StoreCountryRequest $request)
    {
        $country = Country::create([
            'name' => [
                'ar' => request('name_ar'),
                'en' => request('name_en')
            ],
            'iso_code' => request('iso_code'),
            'country_code' => request('country_code'),
            'currency_id' => request('currency_id'),
            'status' => request('status_id')
        ]);

        return redirect()->route('admin.countries.index')->withFlashSuccess(trans('alerts.backend.general.created'));
    }

    public function edit($id)
    {
        $country = Country::findOrFail($id);
        $currencies = Currency::all();

        return view('backend.countries.edit',compact('country','currencies'));
    }

    public function update(UpdateCountryRequest $request,$id)
    {
        $country = Country::findOrFail($id);

        $country->update([
            'name' => [
                'ar' => request('name_ar'),
                'en' => request('name_en')
            ],
            'iso_code' => request('iso_code'),
            'country_code' => request('country_code'),
            'currency_id' => request('currency_id'),
            'status' => request('status_id')
        ]);

        return redirect()->route('admin.countries.index')->withFlashSuccess(trans('alerts.backend.general.updated'));

    }

    public function delete($id)
    {
        $country = Country::findOrFail($id);

        $country->delete();

        return redirect()->route('admin.countries.index')->withFlashSuccess(trans('alerts.backend.general.deleted'));

    }

    public function find()
    {
        $term = trim(request('q'));

        if (empty($term)) {
            return response()->json([]);
        }

        $countries = Country::where(DB::raw('lower(name->"$.en")'), "LIKE", "%".strtolower($term)."%")
                    ->orWhere(DB::raw('lower(name->"$.ar")'), "LIKE", "%".strtolower($term)."%")
                    ->limit(8)
                    ->get();

        $formatted_types = [];

        foreach ($countries as $country) {
            $formatted_types[] = ['id' => $country->id, 'text' => $country->name];
        }

        return response()->json($formatted_types);
    }
}
