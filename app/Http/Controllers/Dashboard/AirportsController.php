<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreAirportRequest;
use App\Http\Requests\UpdateAirportRequest;
use App\Models\Airport;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class AirportsController extends Controller
{
    

    public function index(Request $request)
    {
        
        if($request->ajax()){
            $airports = Airport::orderBy('created_at', 'desc')->with('country')->select('*');

            $datatables = DataTables::of($airports)
            ->addIndexColumn()
            ->editColumn('name',function($q){
                return $q->name;
            })
            ->editColumn('municipality',function($q){
                return $q->municipality;
            })
            ->addColumn('country_name',function($q){
                return $q->country->name;
            })
            ->addColumn('actions',function($q){
                
                $actions = '';
                if(auth()->user()->isAbleTo('airport-delete')){
                    $delete = view('backend.datatable.action-delete')
                                ->with(['route' => route('admin.airports.delete',['airport_id' => $q->id,'allow_delete' => true])])
                                ->render();
                                
                    $actions .= $delete;
                }

                if(auth()->user()->isAbleTo('airport-edit')){
                    $edit = view('backend.datatable.action-edit')
                        ->with(['route' => route('admin.airports.edit', ['airport_id' => $q->id])])
                        ->render();
                    $actions .= $edit;
                }
                
                return $actions;

            });

            if (request('name') != '') {

                $airports->where(DB::raw('lower(name->"$.en")'), "LIKE", "%".strtolower(request('name'))."%")
                ->orWhere(DB::raw('lower(name->"$.ar")'), "LIKE", "%".strtolower(request('name'))."%");
            }

            if (request('municipality') != '') {

                $airports->where(DB::raw('lower(municipality->"$.en")'), "LIKE", "%".strtolower(request('municipality'))."%")
                ->orWhere(DB::raw('lower(municipality->"$.ar")'), "LIKE", "%".strtolower(request('municipality'))."%");
            }

            if (request('country_id') != '') {
                $airports->where('country_id',request('country_id'));
            }

            if (request('country_code') != '') {
                $airports->where('IATA_code',request('country_code'));
            }

            if (request('from') != '') {
                $airports->whereDate('created_at', '>=', request('from'));
            }

            if (request('to') != '') {
                $airports->whereDate('created_at', '<=', request('to'));
            }
            return $datatables->rawColumns(['actions'])->make(true);
        }

        return view('backend.airports.index');
    }

    public function create()
    {
        return view('backend.airports.create');
    }

    public function store(StoreAirportRequest $request)
    {
        Airport::create([
            'name' => [
                'en' => $request->name_en,
                'ar' => $request->name_ar,
            ],
            'municipality' => [
                'en' => $request->municipality_en,
                'ar' => $request->municipality_ar,
            ],
            'country_id' => $request->country_id,
            'IATA_code' => $request->IATA_code
        ]);

        return redirect()->route('admin.airports.index')->withFlashSuccess(trans('alerts.backend.general.created'));

    }

    public function edit($id)
    {
        $airport = Airport::with('country')->where('id',$id)->firstOrFail();

        return view('backend.airports.edit',compact('airport'));
    }

    public function update(UpdateAirportRequest $request,$id)
    {
        $airport = Airport::findOrFail($id);

        $airport->update([
            'name' => [
                'en' => $request->name_en,
                'ar' => $request->name_ar,
            ],
            'municipality' => [
                'en' => $request->municipality_en,
                'ar' => $request->municipality_ar,
            ],
            'country_id' => $request->country_id,
            'IATA_code' => $request->IATA_code
        ]);

        return redirect()->route('admin.airports.index')->withFlashSuccess(trans('alerts.backend.general.updated'));

    }

    public function delete($id)
    {
        $airport = Airport::findOrFail($id);
        $airport->delete();
        return redirect()->route('admin.airports.index')->withFlashSuccess(trans('alerts.backend.general.deleted'));

    }
}
