<?php

namespace App\Http\Controllers\Dashboard;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCurrencyRequest;
use App\Http\Requests\UpdateCurrencyRequest;
use App\Models\Currency;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CurrenciesController extends Controller
{
    
    public function index(Request $request)
    {
        
        if($request->ajax()){
            $currencies = Currency::orderBy('created_at', 'desc')->select('*');

            $datatables = DataTables::of($currencies)
            ->addIndexColumn()
            ->editColumn('name',function($q){
                return $q->name;
            })
            ->editColumn('status',function($q){
                if ($q->status == 1){
                    return '<span class="label label-lg font-weight-bold label-light-primary label-inline">'.trans('labels.backend.general.active').'</span>';
                }

                return '<span class="label label-lg font-weight-bold label-light-danger label-inline">'.trans('labels.backend.general.unactive').'</span>';

            })
            ->addColumn('actions',function($q){
                
                $actions = '';
                if(auth()->user()->isAbleTo('currency-delete')){
                    $delete = view('backend.datatable.action-delete')
                                ->with(['route' => route('admin.currencies.delete',['currency_id' => $q->id,'allow_delete' => true])])
                                ->render();
                                
                    $actions .= $delete;
                }

                if(auth()->user()->isAbleTo('currency-edit')){
                    $edit = view('backend.datatable.action-edit')
                        ->with(['route' => route('admin.currencies.edit', ['currency_id' => $q->id])])
                        ->render();
                    $actions .= $edit;
                }
                
                return $actions;

            });

            if (request('name') != '') {

                $currencies->where(DB::raw('lower(name->"$.en")'), "LIKE", "%".strtolower(request('name'))."%")
                        ->orWhere(DB::raw('lower(name->"$.ar")'), "LIKE", "%".strtolower(request('name'))."%");

            }

            if (request('currency_code') != '') {
                $currencies->where('currency_code',request('currency_code'));
            }

            if (request('status_id') != '') {
                $currencies->where('status',request('status_id'));
            }

            if (request('from') != '') {
                $currencies->whereDate('created_at', '>=', request('from'));
            }

            if (request('to') != '') {
                $currencies->whereDate('created_at', '<=', request('to'));
            }
            return $datatables->rawColumns(['actions','status'])->make(true);

        }

        return view('backend.currencies.index');

    }

    public function create()
    {
        return view('backend.currencies.create');
    }

    public function store(StoreCurrencyRequest $request)
    {
        Currency::create([
            'name' => [
                'en' => $request->name_en,
                'ar' => $request->name_ar
            ],
            'currency_code' => $request->currency_code,
            'status' => $request->status_id
        ]);

        return redirect()->route('admin.currencies.index')->withFlashSuccess(trans('alerts.backend.general.created'));

    }

    public function edit($id)
    {
        $currency = Currency::findOrFail($id);

        return view('backend.currencies.edit',compact('currency'));
    }

    public function update(UpdateCurrencyRequest $request,$id)
    {
        $currency = Currency::findOrFail($id);

        $currency->update([
            'name' => [
                'en' => $request->name_en,
                'ar' => $request->name_ar
            ],
            'currency_code' => $request->currency_code,
            'status' => $request->status_id
        ]);

        return redirect()->route('admin.currencies.index')->withFlashSuccess(trans('alerts.backend.general.updated'));

    }

    public function delete($id)
    {
        $currency = Currency::findOrFail($id);
        $currency->delete();

        return redirect()->route('admin.currencies.index')->withFlashSuccess(trans('alerts.backend.general.deleted'));

    }
}
