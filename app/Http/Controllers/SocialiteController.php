<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Socialite;

class SocialiteController extends Controller
{
    protected $providers = [ "google", "twitter", "facebook" ];

    public function redirect($provider)
    {
     return Socialite::driver($provider)->redirect();
    }

    public function Callback($provider)
    {
        $userSocial =   Socialite::driver($provider)->user();
        $user = User::where('email',$userSocial->getEmail())->first();

        if($user){
            Auth::login($user);
            return redirect('/');
        }

        $newUser = User::create([
            'name'          => $userSocial->getName(),
            'email'         => $userSocial->getEmail(),
            'provider_id'   => $userSocial->getId(),
            'provider'      => $provider,
        ]);

        Auth::login($newUser);
        return redirect('/');
    }
}
