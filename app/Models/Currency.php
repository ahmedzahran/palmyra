<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Currency extends Model
{
    use HasFactory,HasTranslations;

    protected $guarded = [];
    public $translatable = ['name'];

    public function getNameEnAttribute()
    {
        return $this->getTranslations()['name']['en'];
    }

    public function getNameArAttribute()
    {
        return $this->getTranslations()['name']['ar'];
    }

    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:s');
    }
}
