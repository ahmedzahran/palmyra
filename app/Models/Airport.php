<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Airport extends Model
{
    use HasFactory,HasTranslations;

    public $translatable = ['name','municipality'];
    protected $guarded = [];


    public function country()
    {
        return $this->belongsTo(Country::class,'country_id');
    }

    public function getNameEnAttribute()
    {
        return $this->getTranslations()['name']['en'];
    }

    public function getNameArAttribute()
    {
        return $this->getTranslations()['name']['ar'];
    }

    public function getMunicipalityEnAttribute()
    {
        return $this->getTranslations()['municipality']['en'];
    }

    public function getMunicipalityArAttribute()
    {
        return $this->getTranslations()['municipality']['ar'];
    }

    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:s');
    }
}
