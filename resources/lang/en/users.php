<?php

return [
    'title' => 'users',
    'edit' => 'Edit User',
    'view' => 'View User',
    'create' => 'Create New User'
];