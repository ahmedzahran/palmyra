<?php

return [
    'main' => 'Home',
    'users' => 'Users',
    'countries' => 'Countries',
    'airports' => 'Airports',
    'currencies' => 'Currencies'

];