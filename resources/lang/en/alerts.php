<?php

return array (
  'backend' => 
  array (
    'general' => 
    array (
      'created' => 'Created successfully.',
      'slug_exist' => 'Record with same slug exist, please update the slug.',
      'error' => 'Something went wrong. Try Again',
      'updated' => 'Updated successfully.',
      'deleted' => 'Deleted successfully.',
      'restored' => 'Restored successfully.',
      'cancelled' => 'Update Cancelled.',
      'unverified' => 'Unverified Update files.',
      'backup_warning' => 'Please fill necessary details for backup',
      'delete_warning' => 'You can not delete course. Students are already enrolled. Unpublish the course instead',
      'delete_warning_bundle' => 'You can not delete Bundle. Students are already enrolled. Unpublish the Bundle instead',
      'teacher_delete_warning' => 'You can not delete teacher. Courses are already added. Change the status instead',
      'category_childs_delete_warning' => 'You can not delete category. Categories already added . Change status instead',
      'category_courses_delete_warning' => 'You can not delete category. Courses already added . Change status instead or empty category first',
      'category_bundles_delete_warning' => 'You can not delete category. Bundles already added . Change status instead or empty category first',
      'category_create_parent_warning' => 'You can not create category with this parent already have courses',
      'category_stages_delete_warning' => 'You can not delete category . Stages already added . delete stage first',
      'plan_delete_warning' => 'You can not delete plan . Courses already added . change status instead',
      'type_delete_warning' => 'You can not delete type . Categories already added . change status instead',
      'stage_cats_delete_warning' => 'You can not delete stages . Categories already added . remove category first',
      'stage_levels_delete_warning' => 'You can not delete stages . Levels already added . remove level first',
      'level_stages_delete_warning' => 'You Can not delete levels . Stages Already added . remove stages first',
      'level_collages_delete_warning' => 'You Can not delete levels . Collages Already Added . remove collages first',
      'level_departments_delete_warning' => 'You Can not delete levels . Departments Already added . remove departments first',
      'collage_university_delete_warning' => 'You Can not delete collages . Universities already added . remove universities first',
      'universities_collages_delete_warning' => 'You Can not Delete universities . Collages already added . remove collages first ',
      'collage_departments_delete_warning' => 'You Can not delete collages . Departments already added . remove departments first',
      'department_collages_delete_warning' => 'You Can not delete Departments . Collages Already added . remove collage first',
      'collage_levels_delete_warning' => 'You Can not delete collage . levels Already added . remove levels first',
      'department_levels_delete_warning' => 'You Can not delete department . Levels already added . remove level first',
      'course_not_exist' => 'Course not exist',
      'course_plan_max_lessons_count_exceeded' => 'Your course Plan Max lessons count exceeded',
      'course_plan_max_lessons_storage_exceeded' => 'Your course plan max lessons storage exceeded',
      'course_plan_max_lessons_duration_exceeded' => 'Your course plan max lessons duration exceeded'
    ),
    'roles' => 
    array (
      'created' => 'The role was successfully created.',
      'updated' => 'The role was successfully updated.',
      'deleted' => 'The role was successfully deleted.',
    ),
    'users' => 
    array (
      'cant_resend_confirmation' => 'The application is currently set to manually approve users.',
      'confirmation_email' => 'A new confirmation e-mail has been sent to the address on file.',
      'confirmed' => 'The user was successfully confirmed.',
      'unconfirmed' => 'The user was successfully un-confirmed',
      'created' => 'The user was successfully created.',
      'updated' => 'The user was successfully updated.',
      'deleted' => 'The user was successfully deleted.',
      'updated_password' => 'The user\'s password was successfully updated.',
      'session_cleared' => 'The user\'s session was successfully cleared.',
      'social_deleted' => 'Social Account Successfully Removed',
      'deleted_permanently' => 'The user was deleted permanently.',
      'restored' => 'The user was successfully restored.',
    ),
  ),
  'frontend' => 
  array (
    'contact' => 
    array (
      'sent' => 'Your information was successfully sent. We will respond back to the e-mail provided as soon as we can.',
    ),
    'course' => 
    array (
      'completed' => 'Congratulations! You\'ve successfully completed course. Checkout your certificate in dashboard',
    ),
    'duplicate_course' => 'is already course purchased.',
    'duplicate_bundle' => 'is already bundle purchased.',
  ),
);
