<?php

return [
    'backend' => [
        'general' => [
            'date' => 'Date',
            'search' => 'Search',
            'reset' => 'Reset',
            'sr_no' => 'Sr No.',
            'actions' => 'Actions',
            'from' => 'From',
            'to' => 'To',
            'active' => 'Active',
            'unactive' => 'UnActive'
        ],

        'users' => [
            'title' => 'Users',
            'add_new' => 'Add New User',
            'fields' => [
                'name' => 'Name',
                'email' => 'Email',
                'phone' => 'Phone',
                'provider' => 'Provider',
                'password' => 'Password',
                'password_confirmation' => 'Password Confirmation'
            ]
        ],

        'countries' => [
            'title' => 'Countries',
            'create' => 'Create New Country',
            'view' => 'View Countries',
            'edit' => 'Edit Country',
            'add_new' => 'Add New Country',
            'fields' => [
                'name' => 'Country Name',
                'name_en' => 'Country Name En',
                'name_ar' => 'Country Name Ar',
                'currency' => 'Currency',
                'currency_en' => 'Currency En',
                'currency_ar' => 'Currency Ar',
                'country_code' => 'Country Code',
                'currency_code' => 'Currency Code',
                'iso_code' => 'Iso Code',
                'status' => 'Country Status'

            ]
        ],


        'airports' => [
            'title' => 'Airports',
            'create' => 'Create New Airports',
            'view' => 'View Airports',
            'edit' => 'Edit Airports',
            'add_new' => 'Add New Airports',
            'fields' => [
                'name' => 'Airport Name',
                'name_en' => 'Airport Name En',
                'name_ar' => 'Airport Name Ar',
                'municipality' => 'Municipality',
                'municipality_en' => 'Municipality En',
                'municipality_ar' => 'Municipality Ar',
                'IATA_code' => 'IATA Code'
            ]
        ],

        'currencies' => [
            'title' => 'Currencies',
            'create' => 'Create New Currencies',
            'view' => 'View Currencies',
            'edit' => 'Edit Currencies',
            'add_new' => 'Add New Currencies',
            'fields' => [
                'name' => 'Curreny Name',
                'name_en' => 'Curreny Name En',
                'name_ar' => 'Curreny Name Ar',
                'status' => 'Curreny Status',
                'currency_code' => 'Curreny Code'
            ]
        ],

        'roles' => [
            'fields' => [
                'name' => 'Role Name',
            ]
        ],
        'permissions' => [
            'fields' => [
                'name' => 'Permission Name',
            ]
        ]
    ],
    'frontend' => []
];