@extends('layouts.admin')

@section('title',trans('users.create'))

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-custom gutter-b example example-compact">

            <div class="card-header">
                <h3 class="page-title d-inline">@lang('users.create')</h3>
                <div class="float-right">
                    <a href="{{ route('admin.users.index') }}"
                        class="btn btn-success">@lang('users.view')</a>
                </div>
            </div>

            <form class="form fv-plugins-bootstrap fv-plugins-framework" method="POST" action="{{route('admin.users.store')}}" id="store-user">
                <div class="card-body">
                    @csrf
                    <div class="form-group fv-plugins-icon-container">
                        <label>@lang('labels.backend.users.fields.name')</label>
                        <input type="text" name="name" value="{{old('name')}}" class="form-control">
                        <div class="fv-plugins-message-container"></div>
                    </div>

                    <div class="form-group fv-plugins-icon-container">
                        <label>@lang('labels.backend.users.fields.email')</label>
                        <input type="text" name="email" value="{{old('email')}}" class="form-control">
                        <div class="fv-plugins-message-container"></div>
                    </div>

                    <div class="form-group fv-plugins-icon-container">
                        <label>@lang('labels.backend.users.fields.phone')</label>
                        <input type="text" name="phone" value="{{old('phone')}}" class="form-control">
                        <div class="fv-plugins-message-container"></div>
                    </div>

                    <div class="form-group fv-plugins-icon-container">
                        <label>@lang('labels.backend.users.fields.password')</label>
                        <input type="password" name="password" class="form-control">
                        <div class="fv-plugins-message-container"></div>
                    </div>

                    <div class="form-group fv-plugins-icon-container">
                        <label>@lang('labels.backend.users.fields.password_confirmation')</label>
                        <input type="password" name="password_confirmation" class="form-control">
                        <div class="fv-plugins-message-container"></div>
                    </div>
                    <div class="separator separator-dashed my-5"></div>

                    <div class="form-group">
                        <label>@lang('labels.backend.roles.fields.name')</label>
                        <select name="role_id" class="form-control form-control-solid select2">
                            @foreach($roles as $role)
                                <option  value="{{$role->id}}">{{$role->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="separator separator-dashed my-5"></div>

                    <div class="form-group">
                        <label>@lang('labels.backend.permissions.fields.name')</label>
                        <div class="checkbox-inline">
                            @foreach($permissions as $permission)
                                <label class="checkbox">
                                    <input type="checkbox" value="{{$permission->id}}"  name="perimissions[]">
                                    {{$permission->name}}
                                    <span></span>
                                </label>
                            @endforeach
                        </div>
                    </div>


                </div>

                <div class="card-footer">
					<button type="submit" class="btn btn-primary mr-2">{{trans('strings.backend.general.app_save')}}</button>
				</div>
            </form>

        </div>
    </div>
</div>
@endsection
@push('after-scripts')

<script>
    $(document).ready(function(){
        jsValidateForm('store-user',{
            name: {
                validators: {
                    notEmpty: {
                        message: 'name is required'
                    }
                }
            },
            email: {
                validators: {
                    emailAddress: {
								message: 'The value is not a valid email address'
					}
                }
            },
            phone: {
                validators: {
                    notEmpty: {
                        message: 'phone is required'
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: 'password is required'
                    }
                }
            },
            password_confirmation: {
                validators: {
                    notEmpty: {
                        message: 'password_confirmation is required'
                    }
                }
            },
        });
    })
</script>
@endpush