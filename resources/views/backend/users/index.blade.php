@extends('layouts.admin')

@section('title',trans('users.title'))

@push('after-styles')
    @if(app()->getLocale() == 'en')
        <link href="{{asset('met/assets/plugins/custom/datatables/datatables.bundle.css?v=7.0.4')}}" rel="stylesheet" type="text/css" />
    @else
        <link href="{{asset('met/assets/plugins/custom/datatables/datatables.bundle.rtl.css?v=7.0.4')}}" rel="stylesheet" type="text/css" />

    @endif
@endpush
@section('content')

<div class="card card-custom card-custom gutter-t">
    <div class="card-header py-3">
        <div class="card-title">
            <h3 class="card-label">@lang('labels.backend.users.title')</h3>
        </div>
        <div class="card-toolbar">
            <ul class="nav nav-tabs nav-bold">

                @if(auth()->user()->isAbleTo('user-create'))
                    <li class="nav-item">
                        <a class="btn btn-outline-success font-weight-bold btn-pill" href="{{route('admin.users.create')}}">
                            <i class="flaticon-plus"></i>
                            @lang('labels.backend.users.add_new')
                        </a>
                    </li>
                @endif
                
            </ul>
        </div>

    </div>

    <div class="card-body">
        

    <form class="kt-form kt-form--fit mb-15">
            <div class="row mb-6">
                <div class="col-lg-3 mb-lg-0 mb-6">
                    <label>{{trans('labels.backend.users.fields.name')}}:</label>
                    <input type="text" name="name" class="form-control datatable-input name"  data-col-index="0">
                </div>
                <div class="col-lg-3 mb-lg-0 mb-6">
                    <label>{{trans('labels.backend.users.fields.email')}}:</label>
                    <input type="text" name="email" class="form-control datatable-input email"  data-col-index="0">
                </div>

                <div class="col-lg-3 mb-lg-0 mb-6">
                    <label>{{trans('labels.backend.users.fields.phone')}}:</label>
                    <input type="text" name="phone" class="form-control datatable-input phone"  data-col-index="0">
                </div>

                <div class="col-lg-3 mb-lg-0 mb-6">
                    <label>{{trans('labels.backend.users.fields.provider')}}:</label>
                    <select name="provider_id" class="form-control datatable-input select2 provider_id" data-col-index="6">
                        <option selected></option>
                        <option value="google">google</option>
                        <option value="facebook">facebook</option>
                        <option value="twitter">twitter</option>
                    </select>
                </div>
            </div>
            <div class="row mb-8">
                <div class="col-lg-3 mb-lg-0 mb-6">
                    <label>{{trans('labels.backend.general.date')}}:</label>
                    <div class="input-daterange input-group" id="kt_datepicker">
                        <input type="text" class="form-control datatable-input datepicker from" name="from" placeholder="{{trans('labels.backend.general.from')}}" data-col-index="5">
                        <div class="input-group-append">
                            <span class="input-group-text">
                                <i class="la la-ellipsis-h"></i>
                            </span>
                        </div>
                        <input type="text" class="form-control datatable-input datepicker to" name="to" placeholder="{{trans('labels.backend.general.to')}}" data-col-index="5">
                    </div>
                </div>
            </div>
            <div class="row mt-8">
                <div class="col-lg-12">
                    <button class="btn btn-primary btn-primary--icon" id="kt_search">
                        <span>
                            <i class="la la-search"></i>
                            <span>{{trans('labels.backend.general.search')}}</span>
                        </span>
                    </button>&nbsp;&nbsp;
                    <button class="btn btn-secondary btn-secondary--icon" id="kt_reset">
                        <span>
                            <i class="la la-close"></i>
                            <span>{{trans('labels.backend.general.reset')}}</span>
                        </span>
                    </button>
                </div>
            </div>
        </form>
        <div id="kt_datatable_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
            <div class="row">
                <div class="col-sm-12">
                    <table class="table table-separate  table-checkable dataTable no-footer dtr-inline" role="grid" aria-describedby="kt_datatable2_info" id="myTable">
                        <thead>
                            <tr>
                                <th>@lang('labels.backend.general.sr_no')</th>
                                <th>@lang('labels.backend.users.fields.name')</th>
                                <th>@lang('labels.backend.users.fields.email')</th>
                                <th>@lang('labels.backend.users.fields.phone')</th>
                                <th>@lang('labels.backend.users.fields.provider')</th>
                                <th>@lang('labels.backend.general.date')</th>  
                                <th>@lang('labels.backend.general.actions')</th>
      
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('after-scripts')
    <script>
        $(document).ready(function(){

            $('#myTable').DataTable({
                searching: true,
                processing: true,
                serverSide: true,
                iDisplayLength: 10,
                retrieve: true,
                dom: 'lfBrtip<"actions">',
                
                ajax: {
                    url:"{{ route('admin.users.index') }}",
                    data: function(d){
                        d.name = $('input[name=name]').val();
                        d.provider_id = $(".provider_id option:selected").val();
                        d.from = $('input[name=from]').val();
                        d.to = $('input[name=to]').val();
                        d.email = $('input[name=email]').val();
                        d.phone = $('input[name=phone]').val();
                    } 
                },
                columns: [
                    {
                        data: "DT_RowIndex", name: 'DT_RowIndex',searchable:false,sortable:false
                    },
                    {data: "name", name: 'name'},
                    {data: "email", name: 'email'},
                    {data: "phone", name: 'phone'},
                    {data: "provider", name: 'provider'},
                    {data: "created_at", name: 'created_at'},
                    {data: "actions", name: "actions"}
                ]
            });
        });
    </script>
@endpush