@extends('layouts.admin')

@section('title',trans('users.edit'))

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-custom gutter-b example example-compact">

            <div class="card-header">
                <h3 class="page-title d-inline">@lang('users.edit')</h3>
                <div class="float-right">
                    <a href="{{ route('admin.users.index') }}"
                    class="btn btn-success">@lang('users.view')</a>
                </div>
            </div>

            <form class="form" method="POST" action="{{route('admin.users.update',$user->id)}}" id="update-user">
                <div class="card-body">
                    @csrf
                    <div class="form-group fv-plugins-icon-container">
                        <label>@lang('labels.backend.users.fields.name')</label>
                        <input type="text" name="name" class="form-control" value="{{$user->name}}">
                        <div class="fv-plugins-message-container"></div>
                    </div>

                    <div class="form-group fv-plugins-icon-container">
                        <label>@lang('labels.backend.users.fields.email')</label>
                        <input type="text" name="email" class="form-control" value="{{$user->email}}">
                        <div class="fv-plugins-message-container"></div>
                    </div>

                    <div class="form-group fv-plugins-icon-container">
                        <label>@lang('labels.backend.users.fields.phone')</label>
                        <input type="text" name="phone" class="form-control" value="{{$user->phone}}">
                        <div class="fv-plugins-message-container"></div>
                    </div>

                    <div class="separator separator-dashed my-5"></div>

                    <div class="form-group">
                        <label>@lang('labels.backend.roles.fields.name')</label>
                        <select name="role_id" class="form-control form-control-solid select2">
                            @foreach($roles as $role)
                                <option {{$user->hasRole($role->name) ? 'selected' : ''}} value="{{$role->id}}">{{$role->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="separator separator-dashed my-5"></div>

                    <div class="form-group">
                        <label>@lang('labels.backend.permissions.fields.name')</label>
                        <div class="checkbox-inline">
                            @foreach($permissions as $permission)
                                <label class="checkbox">
                                    <input type="checkbox" {{$user->isAbleTo($permission->name) ? 'checked' : ''}} value="{{$permission->id}}"  name="perimissions[]">
                                    {{$permission->name}}
                                    <span></span>
                                </label>
                            @endforeach
                        </div>
                    </div>


                </div>

                <div class="card-footer">
					<button type="submit" class="btn btn-primary mr-2">{{trans('strings.backend.general.app_save')}}</button>
				</div>
            </form>
        </div>
    </div>
</div>
@endsection
@push('after-scripts')

<script>
    $(document).ready(function(){
        jsValidateForm('update-user',{
            name: {
                validators: {
                    notEmpty: {
                        message: 'name is required'
                    }
                }
            },
            email: {
                validators: {
                    emailAddress: {
						message: 'The value is not a valid email address'
					}
                }
            },
            phone: {
                validators: {
                    notEmpty: {
                        message: 'phone is required'
                    }
                }
            }
        });
    })
</script>
@endpush