@extends('layouts.admin')

@section('title',trans('labels.backend.airports.title'))

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-custom gutter-b example example-compact">

            <div class="card-header">
                <h3 class="page-title d-inline">@lang('labels.backend.airports.edit')</h3>
                <div class="float-right">
                    <a href="{{ route('admin.airports.index') }}"
                        class="btn btn-success">@lang('labels.backend.airports.view')</a>
                </div>
            </div>

            <form class="form fv-plugins-bootstrap fv-plugins-framework" method="POST" action="{{route('admin.airports.update',$airport->id)}}" id="update-airport">
                <div class="card-body">
                    @csrf
                    <div class="form-group fv-plugins-icon-container">
                        <label>@lang('labels.backend.airports.fields.name_en')</label>
                        <input type="text" name="name_en" value="{{$airport->name_en}}" class="form-control">
                        <div class="fv-plugins-message-container"></div>
                    </div>

                    <div class="form-group fv-plugins-icon-container">
                        <label>@lang('labels.backend.airports.fields.name_ar')</label>
                        <input type="text" name="name_ar" value="{{$airport->name_ar}}" class="form-control">
                        <div class="fv-plugins-message-container"></div>
                    </div>

                    <div class="form-group fv-plugins-icon-container">
                        <label>@lang('labels.backend.airports.fields.municipality_en')</label>
                        <input type="text" name="municipality_en" value="{{$airport->municipality_en}}" class="form-control">
                        <div class="fv-plugins-message-container"></div>
                    </div>

                    <div class="form-group fv-plugins-icon-container">
                        <label>@lang('labels.backend.airports.fields.municipality_ar')</label>
                        <input type="text" name="municipality_ar" value="{{$airport->municipality_ar}}" class="form-control">
                        <div class="fv-plugins-message-container"></div>
                    </div>


                    <div class="form-group fv-plugins-icon-container">
                        <label>@lang('labels.backend.airports.fields.IATA_code')</label>
                        <input type="text" name="IATA_code" value="{{$airport->IATA_code}}" class="form-control">
                        <div class="fv-plugins-message-container"></div>
                    </div>

                    <div class="form-group fv-plugins-icon-container">
                        <label>@lang('labels.backend.countries.title')</label>
                        <select name="country_id" class="form-control datatable-input select2 country_id">
                            <option  value="{{$airport->country->id}}" selected>{{$airport->country->name}}</option>
                        </select>
                        <div class="fv-plugins-message-container"></div>
                    </div>


                    <div class="separator separator-dashed my-5"></div>

                </div>

                <div class="card-footer">
					<button type="submit" class="btn btn-primary mr-2">{{trans('strings.backend.general.app_save')}}</button>
				</div>
            </form>

        </div>
    </div>
</div>
@endsection
@push('after-scripts')

<script>
    $(document).ready(function(){
        var search_url = "{{route('admin.countries.find')}}";
        var select = $('.country_id');
        selectSearch(search_url,select)
        jsValidateForm('store-airport',{
            name_en: {
                validators: {
                    notEmpty: {
                        message: 'name_en is required'
                    }
                }
            },
            name_ar: {
                validators: {
                    notEmpty: {
                        message: 'name_ar is required'
                    }
                }
            },

            municipality_en: {
                validators: {
                    notEmpty: {
                        message: 'municipality_en is required'
                    }
                }
            },

            municipality_ar: {
                validators: {
                    notEmpty: {
                        message: 'municipality_ar is required'
                    }
                }
            },

            IATA_code: {
                validators: {
                    notEmpty: {
                        message: 'IATA_code is required'
                    }
                }
            },

            country_id: {
                validators: {
                    notEmpty: {
                        message: 'country_id is required'
                    }
                }
            },

        });
    })
</script>
@endpush