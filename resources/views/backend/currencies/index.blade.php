@extends('layouts.admin')

@section('title',trans('labels.backend.currencies.title'))

@push('after-styles')
    @if(app()->getLocale() == 'en')
        <link href="{{asset('met/assets/plugins/custom/datatables/datatables.bundle.css?v=7.0.4')}}" rel="stylesheet" type="text/css" />
    @else
        <link href="{{asset('met/assets/plugins/custom/datatables/datatables.bundle.rtl.css?v=7.0.4')}}" rel="stylesheet" type="text/css" />

    @endif
@endpush
@section('content')

<div class="card card-custom card-custom gutter-t">
    <div class="card-header py-3">
        <div class="card-title">
            <h3 class="card-label">@lang('labels.backend.currencies.title')</h3>
        </div>
        <div class="card-toolbar">
            <ul class="nav nav-tabs nav-bold">

                @if(auth()->user()->isAbleTo('currency-create'))
                    <li class="nav-item">
                        <a class="btn btn-outline-success font-weight-bold btn-pill" href="{{route('admin.currencies.create')}}">
                            <i class="flaticon-plus"></i>
                            @lang('labels.backend.currencies.add_new')
                        </a>
                    </li>
                @endif
                
            </ul>
        </div>
    </div>

    <div class="card-body">
        <form class="kt-form kt-form--fit mb-15">
            <div class="row mb-6">
                <div class="col-lg-3 mb-lg-0 mb-6">
                    <label>{{trans('labels.backend.currencies.fields.name')}}:</label>
                    <input type="text" name="name" class="form-control datatable-input name"  data-col-index="0">
                </div>
                <div class="col-lg-3 mb-lg-0 mb-6">
                    <label>{{trans('labels.backend.currencies.fields.currency_code')}}:</label>
                    <input type="text" name="currency_code" class="form-control datatable-input currency_code"  data-col-index="0">
                </div>
                <div class="col-lg-3 mb-lg-0 mb-6">
                    <label>{{trans('labels.backend.currencies.fields.status')}}:</label>
                    <select name="status_id" class="form-control datatable-input select2 status_id">
                        <option selected></option>
                        <option value="1">{{trans('labels.backend.general.active')}}</option>
                        <option value="0">{{trans('labels.backend.general.unactive')}}</option>
                    </select>
                </div>
                <div class="col-lg-3 mb-lg-0 mb-6">
                    <label>{{trans('labels.backend.general.date')}}:</label>
                    <div class="input-daterange input-group" id="kt_datepicker">
                        <input type="text" class="form-control datatable-input datepicker from" name="from" placeholder="{{trans('labels.backend.general.from')}}" data-col-index="5">
                        <div class="input-group-append">
                            <span class="input-group-text">
                                <i class="la la-ellipsis-h"></i>
                            </span>
                        </div>
                        <input type="text" class="form-control datatable-input datepicker to" name="to" placeholder="{{trans('labels.backend.general.to')}}" data-col-index="5">
                    </div>
                </div>
            </div>
            <div class="row mt-8">
                <div class="col-lg-12">
                    <button class="btn btn-primary btn-primary--icon" id="kt_search">
                        <span>
                            <i class="la la-search"></i>
                            <span>{{trans('labels.backend.general.search')}}</span>
                        </span>
                    </button>&nbsp;&nbsp;
                    <button class="btn btn-secondary btn-secondary--icon" id="kt_reset">
                        <span>
                            <i class="la la-close"></i>
                            <span>{{trans('labels.backend.general.reset')}}</span>
                        </span>
                    </button>
                </div>
            </div>
        </form>
        <div id="kt_datatable_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
            <div class="row">
                <div class="col-sm-12">
                    <table class="table table-separate  table-checkable dataTable no-footer dtr-inline" role="grid" aria-describedby="kt_datatable2_info" id="myTable">
                        <thead>
                            <tr>
                                <th>@lang('labels.backend.general.sr_no')</th>
                                <th>@lang('labels.backend.currencies.fields.name')</th>
                                <th>@lang('labels.backend.currencies.fields.currency_code')</th>
                                <th>@lang('labels.backend.currencies.fields.status')</th>
                                <th>@lang('labels.backend.general.date')</th>  
                                <th>@lang('labels.backend.general.actions')</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('after-scripts')
    <script>
        $(document).ready(function(){

            $('#myTable').DataTable({
                searching: true,
                processing: true,
                serverSide: true,
                iDisplayLength: 10,
                retrieve: true,
                dom: 'lfBrtip<"actions">',
                
                ajax: {
                    url:"{{ route('admin.currencies.index') }}",
                    data: function(d){
                        d.name = $('input[name=name]').val();
                        d.from = $('input[name=from]').val();
                        d.to = $('input[name=to]').val();
                        d.currency_code = $('input[name=currency_code]').val();
                        d.status_id = $(".status_id option:selected").val();

                    } 
                },
                columns: [
                    {
                        data: "DT_RowIndex", name: 'DT_RowIndex',searchable:false,sortable:false
                    },
                    {data: "name", name: 'name'},
                    {data: "currency_code", name: 'currency_code'},
                    {data: "status", name: 'status'},
                    {data: "created_at", name: 'created_at'},
                    {data: "actions", name: "actions"}
                ]
            });
        });
    </script>
@endpush