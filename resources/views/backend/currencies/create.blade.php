@extends('layouts.admin')

@section('title',trans('labels.backend.currencies.title'))

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-custom gutter-b example example-compact">

            <div class="card-header">
                <h3 class="page-title d-inline">@lang('labels.backend.currencies.create')</h3>
                <div class="float-right">
                    <a href="{{ route('admin.currencies.index') }}"
                        class="btn btn-success">@lang('labels.backend.currencies.view')</a>
                </div>
            </div>

            <form class="form fv-plugins-bootstrap fv-plugins-framework" method="POST" action="{{route('admin.currencies.store')}}" id="store-currency">
                <div class="card-body">
                    @csrf
                    <div class="form-group fv-plugins-icon-container">
                        <label>@lang('labels.backend.currencies.fields.name_en')</label>
                        <input type="text" name="name_en" value="{{old('name_en')}}" class="form-control">
                        <div class="fv-plugins-message-container"></div>
                    </div>

                    <div class="form-group fv-plugins-icon-container">
                        <label>@lang('labels.backend.currencies.fields.name_ar')</label>
                        <input type="text" name="name_ar" value="{{old('name_ar')}}" class="form-control">
                        <div class="fv-plugins-message-container"></div>
                    </div>

                    <div class="form-group fv-plugins-icon-container">
                        <label>@lang('labels.backend.currencies.fields.currency_code')</label>
                        <input type="text" name="currency_code" value="{{old('currency_code')}}" class="form-control">
                        <div class="fv-plugins-message-container"></div>
                    </div>

                    <div class="form-group fv-plugins-icon-container">
                        <label>@lang('labels.backend.currencies.fields.status')</label>
                        <select name="status_id" class="form-control datatable-input select2 status_id">
                            <option selected></option>
                            <option value="1">{{trans('labels.backend.general.active')}}</option>
                            <option value="0">{{trans('labels.backend.general.unactive')}}</option>
                        </select>
                        <div class="fv-plugins-message-container"></div>
                    </div>

                    

                    <div class="separator separator-dashed my-5"></div>

                </div>

                <div class="card-footer">
					<button type="submit" class="btn btn-primary mr-2">{{trans('strings.backend.general.app_save')}}</button>
				</div>
            </form>

        </div>
    </div>
</div>
@endsection
@push('after-scripts')

<script>
    $(document).ready(function(){
        jsValidateForm('store-currency',{
            name_en: {
                validators: {
                    notEmpty: {
                        message: 'name_en is required'
                    }
                }
            },
            name_ar: {
                validators: {
                    notEmpty: {
                        message: 'name_ar is required'
                    }
                }
            },

            currency_code: {
                validators: {
                    notEmpty: {
                        message: 'currency_code is required'
                    }
                }
            },

            status_id: {
                validators: {
                    notEmpty: {
                        message: 'status_id is required'
                    }
                }
            },

        });
    })
</script>
@endpush