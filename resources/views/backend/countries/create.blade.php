@extends('layouts.admin')

@section('title',trans('labels.backend.countries.title'))

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-custom gutter-b example example-compact">

            <div class="card-header">
                <h3 class="page-title d-inline">@lang('labels.backend.countries.create')</h3>
                <div class="float-right">
                    <a href="{{ route('admin.users.index') }}"
                        class="btn btn-success">@lang('labels.backend.countries.view')</a>
                </div>
            </div>

            <form class="form fv-plugins-bootstrap fv-plugins-framework" method="POST" action="{{route('admin.countries.store')}}" id="store-country">
                <div class="card-body">
                    @csrf
                    <div class="form-group fv-plugins-icon-container">
                        <label>@lang('labels.backend.countries.fields.name_en')</label>
                        <input type="text" name="name_en" value="{{old('name_en')}}" class="form-control">
                        <div class="fv-plugins-message-container"></div>
                    </div>

                    <div class="form-group fv-plugins-icon-container">
                        <label>@lang('labels.backend.countries.fields.name_ar')</label>
                        <input type="text" name="name_ar" value="{{old('name_ar')}}" class="form-control">
                        <div class="fv-plugins-message-container"></div>
                    </div>

                    

                    <div class="form-group fv-plugins-icon-container">
                        <label>@lang('labels.backend.countries.fields.iso_code')</label>
                        <input type="text" name="iso_code" value="{{old('iso_code')}}" class="form-control">
                        <div class="fv-plugins-message-container"></div>
                    </div>

                    <div class="form-group fv-plugins-icon-container">
                        <label>@lang('labels.backend.countries.fields.country_code')</label>
                        <input type="text" name="country_code" value="{{old('country_code')}}" class="form-control">
                        <div class="fv-plugins-message-container"></div>
                    </div>

                    <div class="form-group fv-plugins-icon-container">
                        <label>{{trans('labels.backend.currencies.fields.status')}}:</label>
                        <select name="status_id" class="form-control datatable-input select2 status_id">
                            <option selected></option>
                            <option value="1">{{trans('labels.backend.general.active')}}</option>
                            <option value="0">{{trans('labels.backend.general.unactive')}}</option>
                        </select>
                    </div>

                    <div class="form-group fv-plugins-icon-container">
                        <label>@lang('labels.backend.currencies.title')</label>
                        <select name="currency_id" class="form-control datatable-input select2 currency_id">
                            <option selected></option>
                            @foreach($currencies as $currency)
                                <option value="{{$currency->id}}">{{$currency->name}}</option>
                            @endforeach
                        </select>
                        <div class="fv-plugins-message-container"></div>
                    </div>


                    <div class="separator separator-dashed my-5"></div>

                </div>

                <div class="card-footer">
					<button type="submit" class="btn btn-primary mr-2">{{trans('strings.backend.general.app_save')}}</button>
				</div>
            </form>

        </div>
    </div>
</div>
@endsection
@push('after-scripts')

<script>
    $(document).ready(function(){
        jsValidateForm('store-country',{
            name_en: {
                validators: {
                    notEmpty: {
                        message: 'name_en is required'
                    }
                }
            },
            name_ar: {
                validators: {
                    notEmpty: {
                        message: 'name_ar is required'
                    }
                }
            },
            iso_code: {
                validators: {
                    notEmpty: {
                        message: 'iso_code is required'
                    }
                }
            },
            country_code: {
                validators: {
                    notEmpty: {
                        message: 'country_code is required'
                    }
                }
            },

            status_id: {
                validators: {
                    notEmpty: {
                        message: 'status_id is required'
                    }
                }
            },


        });
    })
</script>
@endpush