<!DOCTYPE html>
<html>

<head>
    
   <meta charset="UTF-8" />
    <!-- IE Compatibility Meta -->
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- First Mobile Meta  -->
   <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title></title>
    
    <link href="{{asset('website/css/font.css')}}" rel='stylesheet' type='text/css'>
    <link href="{{asset('website/css/flaticon.css')}}" rel='stylesheet' type='text/css'>
    <link href="{{asset('website/css/flaticon2.css')}}" rel='stylesheet' type='text/css'>
    <link href="{{asset('website/css/flaticon3.css')}}" rel='stylesheet' type='text/css'>



	<link rel="stylesheet" href="{{asset('website/css/animate.css')}}" />
    <link rel="stylesheet" href="{{asset('website/css/bootstrap.css')}}" />
    <link rel="stylesheet" href="{{asset('website/css/bootstrap-rtl.css')}}" />
    <link rel="stylesheet" href="{{asset('website/css/font-awesome.min.css')}}" />
    <link rel="stylesheet" href="{{asset('website/css/jquery.bxslider.css')}}" />
    <link rel="stylesheet" href="{{asset('website/css/owl.carousel.css')}}" />
    <link rel="stylesheet" href="{{asset('website/css/daterangepicker.min.css')}}" />
    <link rel="stylesheet" href="{{asset('website/css/style.css')}}" />
    <link rel="stylesheet" href="{{asset('website/css/responisve.css')}}" />
        
          
   <!--[if lt IE 9]>
       <script src="js/html5shiv.min.js"></script>
       <script src="js/respond.min.js"></script>
   <![endif]-->
  
    
</head>
<body>
    <div class="menuMobile">
        <div class="BgClose"></div>
        <div class="menuContent">
                <div class="headMenu clearfix">
                    <i href="#" class="closeX"><i class="fa fa-close"></i></i>
                </div>
                <ul class="menuRes">
                    <li class="parentLi"><a href="#" class="active"><i class="icon flaticon-hotel"></i> طيران</a></li>
                    <li class="parentLi"><a href="hotels.html"><i class="icon flaticon-plane"></i> فنادق</a></li>
                    <li class="parentLi"><a href="offers.html"><i class="icon flaticon2-tag-black-shape"></i> عروض</a></li>
                </ul>
                <div class="headerToggle clearfix" id="selectPrice">
                    <input type="hidden" class="inputHidden" />
                    <h2 class="titleToggle"><i class=" flaticon-search iconRight"></i><span class="spanVal">USD</span> <i class="iconLeft fa fa-angle-down"></i></h2>
                    <div class="contentToggle">
                        <div class="padding20">
                            <form class="searchList">
                                <input type="text" id="inputFilterPrice2" placeholder="ابحث عن الدولة" />
                                <i class="fa fa-search"></i>
                            </form>
                        </div>
                        <ul class="selectList" id="selectFilterPrice2">
                            <li idselect="selectPrice" value="SAR"><img src="{{asset('website/images/saudi-arabia.png')}}" alt="" /> ريال سعودي <span class="val">SAR</span></li>
                            <li idselect="selectPrice" value="AED"><img src="{{asset('website/images/saudi-arabia.png')}}" alt="" /> درهم اماراتي <span class="val">AED</span></li>
                            <li idselect="selectPrice" value="KWD"><img src="{{asset('website/images/saudi-arabia.png')}}" alt="" />  دينار كويتي <span class="val">KWD</span></li>
                            <li idselect="selectPrice" value="BHD"><img src="{{asset('website/images/saudi-arabia.png')}}" alt="" /> دينار بحريني <span class="val">BHD</span></li>
                            <li idselect="selectPrice" value="USD"><img src="{{asset('website/images/saudi-arabia.png')}}" alt="" /> دولار أمريكي <span class="val">USD</span></li>
                            <li idselect="selectPrice" value="EUR"><img src="{{asset('website/images/saudi-arabia.png')}}" alt="" /> يورو <span class="val">EUR</span></li>
                            <li idselect="selectPrice" value="GBP"><img src="{{asset('website/images/saudi-arabia.png')}}" alt="" /> الجنيه الاسترليني <span class="val">GBP</span></li>
                            <li idselect="selectPrice" value="EGP"><img src="{{asset('website/images/saudi-arabia.png')}}" alt="" /> جنيه مصري <span class="val">EGP</span></li>
                            <li idselect="selectPrice" value="INR"><img src="{{asset('website/images/saudi-arabia.png')}}" alt="" /> روبية هندية <span class="val">INR</span></li>
                        </ul>
                    </div>
                </div>
                <div class="headerToggle clearfix" id="selectLang">
                    <input type="hidden" class="inputHidden" />
                    <h2 class="titleToggle"><i class="iconRight flaticon-translation"></i><span class="spanVal">العربية</span> <i class="iconLeft fa fa-angle-down"></i></h2>
                    <div class="contentToggle">
                        <ul class="selectList">
                            <li idselect="selectLang" value="العربية"><img src="{{asset('website/images/saudi-arabia.png')}}" alt="" /> العربية</li>
                            <li idselect="selectLang" value="ENGLISH"><img src="{{asset('website/images/saudi-arabia.png')}}" alt="" /> ENGLISH</li>
                        </ul>
                    </div>
                </div>
                <div class="headerToggle clearfix" id="selectCountry">
                    <input type="hidden" class="inputHidden" />
                    <h2 class="titleToggle"><i class="iconRight flaticon-global"></i><span class="spanVal">السعودية</span> <i class="iconLeft fa fa-angle-down"></i></h2>
                    <div class="contentToggle">
                        <div class="padding20">
                            <form class="searchList">
                                <input type="text" id="inputFilterCountry2" placeholder="ابحث عن الدولة" />
                                <i class="fa fa-search"></i>
                            </form>
                        </div>
                        <ul class="selectList" id="selectFilterCountry2">
                            <li idselect="selectCountry" value="السعوديه"><img src="{{asset('website/images/saudi-arabia.png')}}" alt="" /> السعوديه</li>
                            <li idselect="selectCountry" value="الامارات"><img src="{{asset('website/images/saudi-arabia.png')}}" alt="" /> الامارات</li>
                            <li idselect="selectCountry" value="قطر"><img src="{{asset('website/images/saudi-arabia.png')}}" alt="" /> قطر</li>
                            <li idselect="selectCountry" value="الكويت"><img src="{{asset('website/images/saudi-arabia.png')}}" alt="" /> الكويت</li>
                            <li idselect="selectCountry" value="البحرين"><img src="{{asset('website/images/saudi-arabia.png')}}" alt="" /> البحرين</li>
                            <li idselect="selectCountry" value="أمريكا"><img src="{{asset('website/images/saudi-arabia.png')}}" alt="" /> أمريكا</li>
                            <li idselect="selectCountry" value="اسبانيا"><img src="{{asset('website/images/saudi-arabia.png')}}" alt="" /> اسبانيا</li>
                            <li idselect="selectCountry" value="مصر"><img src="{{asset('website/images/saudi-arabia.png')}}" alt="" /> مصر</li>
                            <li idselect="selectCountry" value="هولندا"><img src="{{asset('website/images/saudi-arabia.png')}}" alt="" /> هولندا</li>
                            <li idselect="selectCountry" value="اوروبا"><img src="{{asset('website/images/saudi-arabia.png')}}" alt="" /> اوروبا</li>
                        </ul>
                    </div>
                </div>
            
                <a href="#" class="btnLogin" data-toggle="modal"data-target="#login">تسجيل الدخول</a>
            
            </div>    
    </div>

    <div class="transformPage">
    
        <div class="heightHeader"></div>

        <div class="header HeaderFixed">
	   		<div class="container-fluid clearfix">
	   			<a href="#" class="logo"><img src="images/logo.png" alt=""></a>
	   			<ul class="linksMenu tabsbtns clearfix">
	   				<li><a href="#" class="active"><i class="flaticon-hotel"></i> طيران</a></li>
	   				<li><a href="hotels.html"><i class="flaticon-plane"></i> فنادق</a></li>
	   				<li><a href="offers.html"><i class="flaticon2-tag-black-shape"></i> عروض</a></li>
	   			</ul>
	   			@if(!auth()->check())
	   				<a href="#" class="btnLogin" data-toggle="modal" data-target="#login">تسجيل الدخول</a>
					@else
					<a class="btnLogin" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                     </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
	   			@endif
	   			<div class="headerToggle" id="selectPrice">
	   				<input type="hidden" class="inputHidden">
	   				<h2 class="titleToggle"><i class="iconLeft flaticon-search"></i><span class="spanVal">USD</span> <i class="iconRight fa fa-angle-down"></i></h2>
	   				<div class="niceScroll contentToggle" tabindex="5000" style="overflow: hidden; outline: none;">
	   					<div class="padding20">
		   					<form class="searchList">
		   						<input type="text" id="inputFilterPrice" placeholder="ابحث عن الدولة">
		   						<i class="fa fa-search"></i>
		   					</form>
	   					</div>
	   					<ul class="selectList" id="selectFilterPrice">
	   						<li idselect="selectPrice" value="SAR"><img src="{{asset('website/images/saudi-arabia.png')}}" alt=""> ريال سعودي <span class="val">SAR</span></li>
	   						<li idselect="selectPrice" value="AED"><img src="{{asset('website/images/saudi-arabia.png')}}" alt=""> درهم اماراتي <span class="val">AED</span></li>
	   						<li idselect="selectPrice" value="KWD"><img src="{{asset('website/images/saudi-arabia.png')}}" alt="">  دينار كويتي <span class="val">KWD</span></li>
	   						<li idselect="selectPrice" value="BHD"><img src="{{asset('website/images/saudi-arabia.png')}}" alt=""> دينار بحريني <span class="val">BHD</span></li>
	   						<li idselect="selectPrice" value="USD"><img src="{{asset('website/images/saudi-arabia.png')}}" alt=""> دولار أمريكي <span class="val">USD</span></li>
	   						<li idselect="selectPrice" value="EUR"><img src="{{asset('website/images/saudi-arabia.png')}}" alt=""> يورو <span class="val">EUR</span></li>
	   						<li idselect="selectPrice" value="GBP"><img src="{{asset('website/images/saudi-arabia.png')}}" alt=""> الجنيه الاسترليني <span class="val">GBP</span></li>
	   						<li idselect="selectPrice" value="EGP"><img src="{{asset('website/images/saudi-arabia.png')}}" alt=""> جنيه مصري <span class="val">EGP</span></li>
	   						<li idselect="selectPrice" value="INR"><img src="{{asset('website/images/saudi-arabia.png')}}" alt=""> روبية هندية <span class="val">INR</span></li>
	   					</ul>
	   				</div>
	   			</div>
	   			<div class="headerToggle" id="selectLang">
	   				<input type="hidden" class="inputHidden">
	   				<h2 class="titleToggle"><i class="iconLeft flaticon-translation"></i><span class="spanVal">العربية</span> <i class="iconRight fa fa-angle-down"></i></h2>
	   				<div class="niceScroll contentToggle" tabindex="5001" style="overflow: hidden; outline: none;">
	   					<ul class="selectList">
	   						<li idselect="selectLang" value="العربية"><img src="{{asset('website/images/saudi-arabia.png')}}" alt=""> العربية</li>
	   						<li idselect="selectLang" value="ENGLISH"><img src="{{asset('website/images/saudi-arabia.png')}}" alt=""> ENGLISH</li>
	   					</ul>
	   				</div>
	   			</div>
	   			<div class="headerToggle" id="selectCountry">
	   				<input type="hidden" class="inputHidden">
	   				<h2 class="titleToggle"><i class="iconLeft flaticon-global"></i><span class="spanVal">السعودية</span> <i class="iconRight fa fa-angle-down"></i></h2>
	   				<div class="niceScroll contentToggle" tabindex="5002" style="overflow: hidden; outline: none;">
	   					<div class="padding20">
		   					<form class="searchList">
		   						<input type="text" id="inputFilterCountry" placeholder="ابحث عن الدولة">
		   						<i class="fa fa-search"></i>
		   					</form>
	   					</div>
	   					<ul class="selectList" id="selectFilterCountry">
	   						<li idselect="selectCountry" value="السعوديه"><img src="{{asset('website/images/saudi-arabia.png')}}" alt=""> السعوديه</li>
	   						<li idselect="selectCountry" value="الامارات"><img src="{{asset('website/images/saudi-arabia.png')}}" alt=""> الامارات</li>
	   						<li idselect="selectCountry" value="قطر"><img src="{{asset('website/images/saudi-arabia.png')}}" alt=""> قطر</li>
	   						<li idselect="selectCountry" value="الكويت"><img src="{{asset('website/images/saudi-arabia.png')}}" alt=""> الكويت</li>
	   						<li idselect="selectCountry" value="البحرين"><img src="{{asset('website/images/saudi-arabia.png')}}" alt=""> البحرين</li>
	   						<li idselect="selectCountry" value="أمريكا"><img src="{{asset('website/images/saudi-arabia.png')}}" alt=""> أمريكا</li>
	   						<li idselect="selectCountry" value="اسبانيا"><img src="{{asset('website/images/saudi-arabia.png')}}" alt=""> اسبانيا</li>
	   						<li idselect="selectCountry" value="مصر"><img src="{{asset('website/images/saudi-arabia.png')}}" alt=""> مصر</li>
	   						<li idselect="selectCountry" value="هولندا"><img src="{{asset('website/images/saudi-arabia.png')}}" alt=""> هولندا</li>
	   						<li idselect="selectCountry" value="اوروبا"><img src="{{asset('website/images/saudi-arabia.png')}}" alt=""> اوروبا</li>
	   					</ul>
	   				</div>
	   			</div>
	   		
	   			<i class="openMenu fa fa-bars"></i>
	   		</div>
	   </div>

        <div class="content">
            @yield('content')
        </div>

        <div class="linksFooter">
	    	<div class="linksHead">
	    		<div class="container">
		    		<ul class="row tabsbtns" id="tabsLink">
		    			<li class="col-md-3 active" id="tabLink1">
	    					<h2 class="titleLinks">خطوط الطيران</h2>
		    			</li>
		    			<li class="col-md-5" id="tabLink2">
	    					<h2 class="titleLinks">الوجهات الأكثر شهرة</h2>
		    			</li>
		    			<li class="col-md-2" id="tabLink3">
	    					<h2 class="titleLinks">عن بالميرا</h2>
		    			</li>
		    			<li class="col-md-2" id="tabLink444">
	    					<a href="offers.html" class="titleLinks">العروض الأكثر طلباً</a>
		    			</li>
		    		</ul>
	    		</div>
	    	</div>
	    	<div class="container">
	    		<div class="tabsLink">
	    			<div class="tab tabLink1">
			    		<div class="row">
			    			<div class="col-md-3">
			    				<ul class="listLinks clearfix">
									<li><a href="bookingFlight.html">الخطوط الجوية السعودية</a></li>
									<li><a href="bookingFlight.html">طيران ناس</a></li>
			    				</ul>
			    			</div>
			    			<div class="col-md-3">
			    				<ul class="listLinks clearfix">
									<li><a href="bookingFlight.html">طيران نسما</a></li>
									<li><a href="bookingFlight.html">طيران أديل</a></li>
			    				</ul>
			    			</div>
			    			<div class="col-md-3">
			    				<h2 class="titleLinks active hiddenTitle">عن بالميرا</h2>
								<ul class="listLinks clearfix">
									<li><a href="bookingFlight.html">طيران النيل</a></li>
									<li><a href="#">طيران السعودية الخليجية</a></li>

								</ul>
			
			    			</div>
			    			<div class="col-md-3">
								<ul class="listLinks clearfix">
									<li><a href="bookingFlight.html">مصر للطيران</a></li>
									<li><a href="bookingFlight.html">الخطوط الجوية الكويتية</a></li>
								</ul>
								<div class="clearfix">
									<a href="bookingFlight.html" class="more">المزيد +</a>
								</div>
			    			</div>
			    		</div>
			    
	    			</div>
	    			<div class="tab tabLink2">
			    		<div class="row">
			    			<div class="col-md-3">
			    				<ul class="listLinks clearfix">
									<li><a href="bookingFlight.html">فلاي دبي</a></li>
									<li><a href="bookingFlight.html">اير كايرو</a></li>
									<li><a href="bookingFlight.html">الخطوط الجوية البريطانية</a></li>
									<li><a href="bookingFlight.html">الخطوط الجوية بانكوك</a></li>
			    				</ul>
			    			</div>
			    			<div class="col-md-3">
			    				<ul class="listLinks clearfix">
									<li><a href="bookingFlight.html">طيران العربية</a></li>
									<li><a href="bookingFlight.html">الخطوط الجوية السنغافورية</a></li>
									<li><a href="bookingFlight.html">الخطوط الجوية الفلبينية</a></li>
									<li><a href="bookingFlight.html">طيران سيبو باسيفيك</a></li>
			    				</ul>
			    			</div>
			    			<div class="col-md-3">
								<ul class="listLinks clearfix">
									<li><a href="bookingFlight.html">الخطوط الجوية السيرلانكية</a></li>
									<li><a href="bookingFlight.html">الخطوط الملكية المغربية</a></li>
									<li><a href="bookingFlight.html">لوفتهانزا</a></li>
									<li><a href="bookingFlight.html">الخطوط الجوية الإريترية</a></li>
								</ul>
			
			    			</div>
			    			<div class="col-md-3">
								<ul class="listLinks clearfix">
									<li><a href="bookingFlight.html">الطيران الهندي</a></li>
									<li><a href="bookingFlight.html">طيران الشرق الاوسط</a></li>
									<li><a href="bookingFlight.html">طيران الخليج</a></li>
									<li><a href="bookingFlight.html">طيران السلام</a></li>
								</ul>
								<div class="clearfix">
									<a href="bookingFlight.html" class="more">المزيد +</a>
								</div>
			    			</div>
			    		</div>
			    
	    			</div>
	    			<div class="tab tabLink3">
			    		<div class="row">
			    			<div class="col-md-3">
			    				<ul class="listLinks clearfix">
									<li><a href="bookingFlight.html">من نحن</a></li>
									<li><a href="bookingFlight.html">تواصل معنا</a></li>
			    				</ul>
			    			</div>
			    			<div class="col-md-3">
			    				<ul class="listLinks clearfix">
									<li><a href="bookingFlight.html">تسجيل جديد</a></li>
									<li><a href="bookingFlight.html">تسجيل الدخول</a></li>
			    				</ul>

			    			</div>
			    			<div class="col-md-3">
								<ul class="listLinks clearfix">
									<li><a href="bookingFlight.html">كوبانات الخصم</a></li>
									<li><a href="bookingFlight.html">القائمة المفضلة</a></li> 
								</ul>
			
			    			</div>
			    			<div class="col-md-3">
								<ul class="listLinks clearfix">
									<li><a href="bookingFlight.html">الشروط والأحكام</a></li>
									<li><a href="bookingFlight.html">الأسئلة الشائعة</a></li>
								</ul>
								<div class="clearfix">
									<a href="bookingFlight.html" class="more">المزيد +</a>
								</div>
			    			</div>
			    		</div>
			    
	    			</div>
	    			<div class="tab tabLink4">
			    		<div class="row">
			    			<div class="col-md-3">
			    				<ul class="listLinks linksFloat clearfix">
									<li><a href="bookingFlight.html">السعودية</a></li>
									<li><a href="bookingFlight.html">الإمارات</a></li>
									<li><a href="bookingFlight.html">البحرين</a></li>
									<li><a href="bookingFlight.html">أمريكا</a></li>

			    				</ul>
			    			</div>
			    			<div class="col-md-3">
			    				<ul class="listLinks linksFloat clearfix">
									<li><a href="bookingFlight.html">المادليف</a></li>
									<li><a href="bookingFlight.html">الغردقة</a></li>
									<li><a href="bookingFlight.html">مكة</a></li>
									<li><a href="bookingFlight.html">الرياض</a></li>
			    				</ul>
			    			</div>
			    			<div class="col-md-3">
								<ul class="listLinks linksFloat clearfix">
									<li><a href="bookingFlight.html">السعودية</a></li>
									<li><a href="bookingFlight.html">الإمارات</a></li>
									<li><a href="bookingFlight.html">البحرين</a></li>
									<li><a href="bookingFlight.html">أمريكا</a></li>
								</ul>
			
			    			</div>
			    			<div class="col-md-3">
								<ul class="listLinks linksFloat clearfix">
									<li><a href="bookingFlight.html">المادليف</a></li>
									<li><a href="bookingFlight.html">الغردقة</a></li>
									<li><a href="bookingFlight.html">مكة</a></li>
									<li><a href="bookingFlight.html">الرياض</a></li>
								</ul>
								<div class="clearfix">
									<a href="bookingFlight.html" class="more">المزيد +</a>
								</div>
			    			</div>
			    		</div>
			    
	    			</div>

	    		</div>

	    	</div>
	    	
	    </div>

        <div class="footerSite">
	    	<div class="bgFooter"></div>
	    	<div class="container">
	    		<h2 class="title">القائمة الإخبارية</h2>
	    		<span class="desc">الثقة المتبادلة بيننا وبين عملاءنا هي سر نجاحنا</span>
	    		<center>
		    		<form class="subscripe clearfix">
		    			<input type="text" placeholder="يسعدنا إضافة بريدك الإلكتروني">
		    			<button>ارسال الأن</button>
		    		</form>
	    		</center>
	    		<p class="copyrights">جميع الحقوق محفوظة لموقع بالميرا</p>
	    		<center>
		    		<ul class="SocialFooter clearfix">
		    			<li><a href="#" class="fa fa-facebook"></a></li>
		    			<li><a href="#" class="fa fa-twitter"></a></li>
		    			<li><a href="#" class="fa fa-youtube"></a></li>
		    			<li><a href="#" class="fa fa-linkedin"></a></li>
		    			<li><a href="#" class="fa fa-instagram"></a></li>
		    		</ul>
	    		</center>
	    		<center>
		    		<ul class="listPayment clearfix">
		    			<li><img src="{{asset('website/images/payment1.png')}}" alt=""></li>
		    			<li><img src="{{asset('website/images/payment2.png')}}" alt=""></li>
		    			<li><img src="{{asset('website/images/payment3.png')}}" alt=""></li>
		    		</ul>
	    		</center>
	    		<center>
	    			<p class="servers">صنع في مركز الخوادم الرقمية</p>
	    		</center>
	    	</div>
	    </div>
    </div>

    <!-- start login modals -->
    <div id="login" class="login-modal modal fade" role="dialog" aria-hidden="true" style="display: none;">
          <div class="modal-dialog">
                        
            <div class="modal-content">
    
                <div class="modal-body clearfix">
    
                    <button type="button" class="close fa fa-close" data-dismiss="modal"></button>
                    
                    <div class="rightSection">
                    	<div class="bgFooter"></div>
                    	<h2 class="title">في بالميرا تجد الراحة في السفر حول العالم</h2>
                    	<ul class="listLogin">
                    		<li><i class="flaticon3-travel-1"></i> أفضل الفنادق والأماكن حول العالم</li>
                    		<li><i class="flaticon3-shield"></i> حفظ معلومات بنظام تقني آمن وسرية</li>
                    		<li><i class="flaticon-calendar"></i> سرعة الحجز والمتابعة بشكل دقيق</li>
                    		<li><i class="flaticon2-tag-black-shape"></i> عروض وبأسعار مخفضة على مدار اليوم</li>
                    		<li><i class="flaticon-setup"></i> ادراة حسابك وسفرك بكل سهولة</li>
                    	</ul>
                    </div>
                    <div class="leftSection">
				    	<div class="content">
				    		<h2 class="title">تسجيل الدخول</h2>
							@if(count($errors) > 0)
								<div class="alert alert-danger">
									@foreach($errors->all() as $error)
										{{$error}}
									@endforeach
								</div>	
							@endif
				    		<a href="#" class="btnLogin twitter effect-btn" data-animation="ripple"><i class="fa fa-twitter"></i> التسجيل بواسطة تويتر</a>
				    		<a href="#" class="btnLogin facebook effect-btn" data-animation="ripple"><i class="fa fa-facebook"></i> التسجيل بواسطة فيس بوك</a>
				    		<a href="{{ url('auth/google') }}" class="btnLogin googlePlus effect-btn" data-animation="ripple"><i class="fa fa-google-plus"></i> التسجيل بواسطة جوجل</a>
				    		<a class="btnLogin effect-btn openLoginBtn" data-animation="ripple"><i class="flaticon-telephone"></i> التسجيل بواسطة الجوال</a>
				    		<center>
				    			<a class="btnForget openForgetBtn">نسيت كلمة المرور ؟</a>
				    		</center>
				    		<div class="loginFooter">
				    			<h2 class="titleFooter">ليس لديك حساب ؟</h2>
				    			<a class="btnFooter openSingUpBtn">سجل الآن مجاناً</a>
				    		</div>
				   			
				    		<form class="formLogin openLogin" action="{{ route('login') }}" method="POST">
				    			<i class="fa fa-angle-left closeForm closeFormLogin"></i>
								@csrf

				    			<h2 class="title">تسجيل الدخول</h2>
								
				    			<div class="inputStyle">
				    				<input name="phone" type="text">
				    				<span>رقم الجوال</span>
				    				<i class="flaticon-telephone icon"></i>
				    			</div>
				    			<div class="inputStyle">
				    				<i class="flaticon-visibility-button btnVisib" onclick="myFunction('myInputPassword1')"></i>
				    				<input name="password" type="password" id="myInputPassword1">
				    				<span>كلمة المرور</span>
				    				<i class="flaticon-telephone icon"></i>
				    			</div>
				    			<!-- data-toggle="modal" data-target="#loginModal"  data-animation="ripple" -->
								<button type="submit" class="btnStyle effect-btn">تسجيل الدخول</button>
					    		<center>
					    			<a href="#" class="btnForget openForgetBtn">نسيت كلمة المرور ؟</a>
					    		</center>
					    		<div class="loginFooter">
					    			<h2 class="titleFooter">ليس لديك حساب ؟</h2>
					    			<a class="btnFooter openSingUpBtn">سجل الآن مجاناً</a>
					    		</div>
				    			
				    		</form>

				    		<form class="formLogin openSingUp" method="post" action="{{ route('register') }}">
								@csrf

				    			<i class="fa fa-angle-left closeForm closeFormSingUp"></i>
				    			<h2 class="title">إنشاء حساب جديد</h2>
				    			
				    			<div class="inputStyle">
									
				    				<input name="name" type="text">
				    				<span> اﻷسم </span>
				    				<i class="flaticon-telephone icon"></i>
				    			</div>
								<div class="inputStyle">
				    				<input name="phone" type="text">
				    				<span>أضف رقم جوالك</span>
				    				<i class="flaticon-telephone icon"></i>
				    			</div>
				    			<div class="inputStyle">
				    				<i class="flaticon-visibility-button btnVisib" onclick="myFunction('myInputPassword2')"></i>
				    				<input name="password" type="password" id="myInputPassword2">
				    				<span>كلمة المرور</span>
				    				<i class="flaticon-telephone icon"></i>
				    			</div>
								<div class="inputStyle">
				    				<i class="flaticon-visibility-button btnVisib" onclick="myFunction('myInputPassword2')"></i>
				    				<input name="password_confirmation" type="password" id="myInputPassword2">
				    				<span>كلمة المرور</span>
				    				<i class="flaticon-telephone icon"></i>
				    			</div>
				    			<button class="effect-btn btnStyle" data-animation="ripple">التسجيل الآن</button>
					    		<label class="switch clearfix">
					    			<h2 class="titleSwitch">تذكرني دائماً</h2>
									<label class="switchBtn">
									  <input type="checkbox">
									  <span class="sliderBtn round"></span>
									</label>
					    		</label>
					    		
					    		<div class="loginFooter">
					    			<h2 class="titleFooter">لدي حساب سابقاً ؟</h2>
					    			<a class="btnFooter openLoginBtn">تسجيل الدخول</a>
					    		</div>
				    			
				    			
				    		</form>
					   
				    		<form class="formLogin openForget">
				    			
				    			<i class="fa fa-angle-left closeForm closeFormForget"></i>
				    			<h2 class="title">استرداد كلمة المرور</h2>
				    			<div class="inputStyle">
				    				<input type="text">
				    				<span>رقم الجوال</span>
				    				<i class="flaticon-telephone icon"></i>
				    			</div>
				    			<div class="inputStyle">
				    				<i class="flaticon-visibility-button btnVisib" onclick="myFunction('myInputPassword3')"></i>
				    				<input type="password" id="myInputPassword3">
				    				<span>كلمة المرور الجديدة</span>
				    				<i class="flaticon-telephone icon"></i>
				    			</div>
				    			<button class="btnStyle effect-btn" data-animation="ripple">تغيير كلمة المرور</button>
				    			
				    			
				    		</form>
					   
				    		
				    		
				    	</div>

				   
                    </div>
                    
                    
                </div>
            </div>
        </div>
    </div>
    <!-- end login modals -->
    <!-- start form contact modals -->
    <div id="formContact" class="login-modal formContact-modal modal fade" role="dialog">
          <div class="modal-dialog">
                        
            <div class="modal-content">
    
                <div class="modal-body clearfix">
    
                    <button type="button" class="close fa fa-close" data-dismiss="modal"></button>
                    
                    <div class="rightSection">
                    	<div class="bgFooter"></div>
                    	<h2 class="title">في بالميرا تجد الراحة في السفر حول العالم</h2>
                    	<ul class="listLogin">
                    		<li><i class="flaticon3-travel-1"></i> أفضل الفنادق والأماكن حول العالم</li>
                    		<li><i class="flaticon3-shield"></i> حفظ معلومات بنظام تقني آمن وسرية</li>
                    		<li><i class="flaticon-calendar"></i> سرعة الحجز والمتابعة بشكل دقيق</li>
                    		<li><i class="flaticon2-tag-black-shape"></i> عروض وبأسعار مخفضة على مدار اليوم</li>
                    		<li><i class="flaticon-setup"></i> ادراة حسابك وسفرك بكل سهولة</li>
                    	</ul>
                    </div>
                    <div class="leftSection">
				    	<div class="content">
    
						  	<div class="contactForm">
						  		
						  		<div class="content">
						  			<img src="{{asset('website/images/sent(1).png')}}" alt="">
						  			<h2 class="titleContact">نستقبل جميع استفساراتكم 
									واسئلتكم على مدار الوقت</h2>
									<input type="text" placeholder="الإسم :">
									<input type="email" placeholder="البريد الإلكتروني :">
									<input type="number" placeholder="رقم الجوال :">
									<textarea placeholder="تفاصيل الرسالة :"></textarea>
									<button class="effect-btn" data-animation="ripple">ارسال الأن</button>
						  		</div>
						  		
						  	</div>
				    		
				    	</div>

				   
                    </div>
                    
                    
                </div>
            </div>
        </div>
    </div>

    <!-- end form contact modals -->
    <!-- start register modals -->

    <div id="loginModal" class="loginModal modal fade" role="dialog">
          <div class="modal-dialog">
                        
            <div class="modal-content">
    
                <div class="modal-body">
    				<h2 class="title"> جاري تسجيل الدخول <i class="flaticon-loading fa-spin"></i></h2>
    				
                </div>
            </div>
        </div>
    </div>
    <!-- end register modals -->

    <script src="{{asset('website/js/jquery-1.11.2.min.js')}}"></script>
    <script src="{{asset('website/js/jquery-ui.min.js')}}"></script>
    <script src="{{asset('website/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('website/js/jquery.bxslider.min.js')}}"></script>
    <script src="{{asset('website/js/owl.carousel.js')}}"></script>
    <script src="{{asset('website/js/wow.min.js')}}"></script>
    <script src="{{asset('website/js/scrollIt.min.js')}}"></script>
    <script src="{{asset('website/js/jquery.nicescroll.min.js')}}"></script>
    <script src="{{asset('website/js/moment.min.js')}}"></script>
    <script src="{{asset('website/js/jquery.raty.js')}}"></script>
    <script src="{{asset('website/js/intlTelInput.js')}}"></script>
    <script src="{{asset('website/js/jquery.daterangepicker.min.js')}}"></script>
    <script src="{{asset('website/js/jQRangeSlider-min.js')}}"></script>
    <script src="{{asset('website/js/custom.js')}}"></script>

	@if(count($errors) > 0 )
		<script>
			$('#login').modal().show();
		</script>
	@endif
    @stack('script')
</body>
</html>