@extends('layouts.website')

@section('content')

<div class="searchContent">
	<div class="tabsSearch">
		<div class="tab1 tab">
			<div class="bg">
			    <img src="{{asset('website/images/searchBg.png')}}" alt="" class="bgSearch">
			</div>
			<h2 class="titleSection">ابحث في عدد من الفنادق الفخمة</h2>
			<span class="descSection">فنادق , ايجارات , فلل , شاليهات...</span>
		</div>
		<div class="tab2 tab">
			<div class="bg">
			    <img src="{{asset('website/images/9129.png')}}" alt="" class="bgSearch">
			</div>
			<h2 class="titleSection">استمتع بجولة السفر حول العالم</h2>
			<span class="descSection">ابحث عن رحتلك في جميع مناطق العالم</span>
		</div>
	</div>
	
	<div class="container">
		<ul class="tabsbtns clearfix" id="tabsSearch">
			<li id="tab1" class="active"><i class="flaticon-hotel"></i> فنادق</li>
			<li id="tab2"><i class="flaticon-plane"></i> طيران</li>
		</ul>
		<div class="tabsSearch">
            @include('frontend.includes.search.hotels')
            @include('frontend.includes.search.flights')
        </div>
	</div>
</div>


<div class="interfaces">
	<div class="container">
		<h2 class="title">واجهات ذات أهمية</h2>
		<a href="hotels.html" class="more"><i class="flaticon-plus-symbol"></i> المزيد</a>
		<div class="row">
			<div class="col-md-3">
				<div class="item">
					<img src="{{asset('website/images/shutterstock_1033351084.png')}}" alt="">
					<div class="mask">
						<h2 class="titleItem">مدينة الكويت</h2>
						<a href="hotels.html" class="btnitem" data-toggle="modal" data-target="#formContact">للحجز الآن</a>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="item">
					<img src="{{asset('website/images/a49124d9-5f62-47a5-b5ec-8dd3a3066b30-shutterstock-1420728554.png')}}" alt="">
					<div class="mask">
						<h2 class="titleItem">باريس عاصمة فرنسا</h2>
						<a href="hotels.html" class="btnitem" data-toggle="modal" data-target="#formContact">للحجز الآن</a>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="item">
					<img src="{{asset('website/images/ColosseumInRomeItaly_Shutterstock_1536x864.png')}}" alt="">
					<div class="mask">
						<h2 class="titleItem">ايطاليا عاصمة روما</h2>
						<a href="hotels.html" class="btnitem" data-toggle="modal" data-target="#formContact">للحجز الآن</a>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="item">
					<img src="{{asset('website/images/2249521159_34db6e8ee9_b.png')}}" alt="">
					<div class="mask">
						<h2 class="titleItem">السعودية - مكة</h2>
						<a href="hotels.html" class="btnitem" data-toggle="modal" data-target="#formContact">للحجز الآن</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="hotels">
    <div class="container">
        <h2 class="title">أفضل الفنادق</h2>
        <span class="desc">العديد من الأشخاص حول العالم على زيارة دائمة لهذة الفنادق</span>
        <div id="owlHotels" class="owlHotels Owl owl-carousel owl-theme" style="opacity: 1; display: block;">
            <div class="owl-wrapper-outer">
                <div class="owl-wrapper" style="width: 2280px; left: 0px; display: block; transition: all 1000ms ease 0s; transform: translate3d(0px, 0px, 0px);">
                    <!-- start hotel item -->
                    <div class="owl-item" style="width: 228px;">
                        <div class="item">
                            <div class="itemHotels">
                                <div class="mask">
                                    <img src="{{asset('website/images/hotel2.png')}}" alt="">
                                    <div class="maskContent">
                                        <i class="fa fa-heart-o heart"></i>
                                        <div class="shareDiv">
                                            <i class="flaticon-network openShare"></i>
                                            <ul class="listSocial" style="">
                                                <li><a href="#" class="facebook"><i class="fa fa-facebook"></i> Facebook <span>20</span></a></li>
                                                <li><a href="#" class="twitter"><i class="fa fa-twitter"></i> Twitter <span>20</span></a></li>
                                                <li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i> LinkedIn <span>20</span></a></li>
                                                <li><a href="#" class="google-plus"><i class="fa fa-google-plus"></i> Google+ <span>20</span></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="details">
                                    <a href="hotel-details.html" class="titleHotel">فندق ذا لايت بينانج</a>
                                    <div class="clearfix">
                                        <div class="rate" style="cursor: pointer;">
                                        <img alt="1" src="{{asset('website/images/star-on.png')}}" title="bad">&nbsp;
                                       
                                        <input name="score" type="hidden" value="2"></div>
                                        <span class="rateText">(310 تقييم)</span>
                                    </div>
                                    <div class="clearfix">
                                        <span class="price"><i class="flaticon-map"></i>مكة</span>
                                        <span class="price"><i class="flaticon-price"></i>SAR 764.64</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end hotel item -->
                    
                </div>
            </div>
        </div>
    </div>
</div>


<div class="appMobile">
	<div class="container">
	    <div class="row">
	    	<div class="col-md-5">
	    				<img src="{{asset('website/images/mobile.png')}}" class="imageMobile" alt="">
	    	</div>
	    	<div class="col-md-7">
	    		<h2 class="title">التطبيق متوفر الآن</h2>
	    		<span class="desc">تمتع بالحصول على أفضل العروض والأماكن حول العالم</span>
	    		<div class="clearfix">
	    			<a href="#" class="appLogo"><img src="{{asset('website/images/googlePlay.png')}}" class="appLogo" alt=""></a>
	    			<a href="#" class="appLogo"><img src="{{asset('website/images/appleApp.png')}}" alt=""></a>
	    		</div>
	    	</div>
	    </div>
	</div>
</div>


<div class="phoneSection">
	<div class="container">
	    <img src="{{asset('website/images/phoneImage.png')}}" alt="">
	    <h2 class="title">24 / 7 خدمة العملاء</h2>
	    <span class="desc">نحن معك على مدار الوقت على الرقم التالي</span>
	    <a href="tel:01234567890" class="numb">01234567890</a>
	</div>
</div>

<iframe class="mapBranch" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d927764.3550487261!2d47.38299600868243!3d24.724150391445495!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e2f03890d489399%3A0xba974d1c98e79fd5!2z2KfZhNix2YrYp9i2INin2YTYs9i52YjYr9mK2Kk!5e0!3m2!1sar!2seg!4v1571426803980!5m2!1sar!2seg" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
@endsection

@push('script')
<script>

    	$('.rate').raty({
		score:2,
		starOff: '{{asset("website/images/star-off.png")}}',
		starOn: '{{asset("website/images/star-on.png")}}',
		number:7
	});
	
</script>
@endpush