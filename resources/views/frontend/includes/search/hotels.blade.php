<div class="tab1 tab" style="display: block;">
				<div class="row">
					<div class="col-md-4">
					    <div class="locationStart" id="selectCity">
					    	<input type="hidden" class="inputHidden">
					    	<div class="Start">
					    		<i class="iconAngle fa fa-angle-down"></i>
					    		<i class="iconRight flaticon-map"></i>
					    		<span>حدد المكان</span>
					    		<span class="title spanVal">المملكة العربية السعودية</span>
					    	</div>
					   		<div class="niceScroll contentToggle" tabindex="5003" style="overflow: hidden; outline: none;">
					   			<div class="padding20">
						   			<form class="searchList">
						   				<input type="text" id="selectFilterCity" placeholder="ابحث عن الدولة">
						   				<i class="fa fa-search"></i>
						   			</form>
					   			</div>
					   				    <ul class="selectList" id="selectFilterCity">
					   						<li idselect="selectCity" value="السعوديه"><img src="images/saudi-arabia.png" alt=""> السعوديه</li>
					   						<li idselect="selectCity" value="الامارات"><img src="images/saudi-arabia.png" alt=""> الامارات</li>
					   						<li idselect="selectCity" value="قطر"><img src="images/saudi-arabia.png" alt=""> قطر</li>
					   						<li idselect="selectCity" value="الكويت"><img src="images/saudi-arabia.png" alt=""> الكويت</li>
					   						<li idselect="selectCity" value="البحرين"><img src="images/saudi-arabia.png" alt=""> البحرين</li>
					   						<li idselect="selectCity" value="أمريكا"><img src="images/saudi-arabia.png" alt=""> أمريكا</li>
					   						<li idselect="selectCity" value="اسبانيا"><img src="images/saudi-arabia.png" alt=""> اسبانيا</li>
					   						<li idselect="selectCity" value="مصر"><img src="images/saudi-arabia.png" alt=""> مصر</li>
					   						<li idselect="selectCity" value="هولندا"><img src="images/saudi-arabia.png" alt=""> هولندا</li>
					   						<li idselect="selectCity" value="اوروبا"><img src="images/saudi-arabia.png" alt=""> اوروبا</li>
					   					</ul>
					   		</div>
					    </div>
					</div>
					<div class="col-md-3">
					    <div class="locationStart">
					    	<div class="Start">
					    		<i class="iconAngle fa fa-angle-down"></i>
					    		<i class="iconRight flaticon-calendar"></i>
					    		<span>ذهاب وعودة</span>
					    		<span class="title">28 سبتمبر - 29 سبتمبر</span>
					    	</div>
					    	<div class="contentToggle contentTimer" style="">
							  	<input type="text" id="openDateFirst" class="openDate">
							  	<div id="date-range-container"><div class="date-picker-wrapper no-shortcuts  no-topbar  inline-wrapper no-gap single-month" unselectable="on" style="user-select: none;"><div class="month-wrapper" style="width: 100px;">   <table class="month1" cellspacing="0" border="0" cellpadding="0">       <thead>           <tr class="caption">               <th>                   <span class="prev">&lt;                   </span>               </th>               <th colspan="5" class="month-name"><div class="month-element">january</div> <div class="month-element">2021</div></th>               <th><span class="next">&gt;</span>               </th>           </tr>           <tr class="week-name"><th>su</th><th>mo</th><th>tu</th><th>we</th><th>th</th><th>fr</th><th>sa</th>       </tr></thead>       <tbody><tr><td><div time="1609081659881" data-tooltip="" class="day lastMonth  valid ">27</div></td><td><div time="1609168059881" data-tooltip="" class="day lastMonth  valid ">28</div></td><td><div time="1609254459881" data-tooltip="" class="day lastMonth  valid ">29</div></td><td><div time="1609340859881" data-tooltip="" class="day lastMonth  valid ">30</div></td><td><div time="1609427259881" data-tooltip="" class="day lastMonth  valid ">31</div></td><td><div time="1609513659881" data-tooltip="" class="day toMonth  valid ">1</div></td><td><div time="1609600059881" data-tooltip="" class="day toMonth  valid ">2</div></td></tr><tr><td><div time="1609686459881" data-tooltip="" class="day toMonth  valid real-today">3</div></td><td><div time="1609772859881" data-tooltip="" class="day toMonth  valid ">4</div></td><td><div time="1609859259881" data-tooltip="" class="day toMonth  valid ">5</div></td><td><div time="1609945659881" data-tooltip="" class="day toMonth  valid ">6</div></td><td><div time="1610032059881" data-tooltip="" class="day toMonth  valid ">7</div></td><td><div time="1610118459881" data-tooltip="" class="day toMonth  valid ">8</div></td><td><div time="1610204859881" data-tooltip="" class="day toMonth  valid ">9</div></td></tr><tr><td><div time="1610291259881" data-tooltip="" class="day toMonth  valid ">10</div></td><td><div time="1610377659881" data-tooltip="" class="day toMonth  valid ">11</div></td><td><div time="1610464059881" data-tooltip="" class="day toMonth  valid ">12</div></td><td><div time="1610550459881" data-tooltip="" class="day toMonth  valid ">13</div></td><td><div time="1610636859881" data-tooltip="" class="day toMonth  valid ">14</div></td><td><div time="1610723259881" data-tooltip="" class="day toMonth  valid ">15</div></td><td><div time="1610809659881" data-tooltip="" class="day toMonth  valid ">16</div></td></tr><tr><td><div time="1610896059881" data-tooltip="" class="day toMonth  valid ">17</div></td><td><div time="1610982459881" data-tooltip="" class="day toMonth  valid ">18</div></td><td><div time="1611068859881" data-tooltip="" class="day toMonth  valid ">19</div></td><td><div time="1611155259881" data-tooltip="" class="day toMonth  valid ">20</div></td><td><div time="1611241659881" data-tooltip="" class="day toMonth  valid ">21</div></td><td><div time="1611328059881" data-tooltip="" class="day toMonth  valid ">22</div></td><td><div time="1611414459881" data-tooltip="" class="day toMonth  valid ">23</div></td></tr><tr><td><div time="1611500859881" data-tooltip="" class="day toMonth  valid ">24</div></td><td><div time="1611587259881" data-tooltip="" class="day toMonth  valid ">25</div></td><td><div time="1611673659881" data-tooltip="" class="day toMonth  valid ">26</div></td><td><div time="1611760059881" data-tooltip="" class="day toMonth  valid ">27</div></td><td><div time="1611846459881" data-tooltip="" class="day toMonth  valid ">28</div></td><td><div time="1611932859881" data-tooltip="" class="day toMonth  valid ">29</div></td><td><div time="1612019259881" data-tooltip="" class="day toMonth  valid ">30</div></td></tr><tr><td><div time="1612105659881" data-tooltip="" class="day toMonth  valid ">31</div></td><td><div time="1612192059881" data-tooltip="" class="day nextMonth  valid ">1</div></td><td><div time="1612278459881" data-tooltip="" class="day nextMonth  valid ">2</div></td><td><div time="1612364859881" data-tooltip="" class="day nextMonth  valid ">3</div></td><td><div time="1612451259881" data-tooltip="" class="day nextMonth  valid ">4</div></td><td><div time="1612537659881" data-tooltip="" class="day nextMonth  valid ">5</div></td><td><div time="1612624059881" data-tooltip="" class="day nextMonth  valid ">6</div></td></tr></tbody>   </table><div class="dp-clearfix"></div><div class="time"><div class="time1"></div><div class="time2"></div></div><div class="dp-clearfix"></div></div><div class="footer"></div><div class="date-range-length-tip"></div></div><div class="date-picker-wrapper no-shortcuts  no-topbar  inline-wrapper no-gap single-month" unselectable="on" style="user-select: none;"><div class="month-wrapper" style="width: 100px;">   <table class="month1" cellspacing="0" border="0" cellpadding="0">       <thead>           <tr class="caption">               <th>                   <span class="prev">&lt;                   </span>               </th>               <th colspan="5" class="month-name"><div class="month-element">january</div> <div class="month-element">2021</div></th>               <th><span class="next">&gt;</span>               </th>           </tr>           <tr class="week-name"><th>su</th><th>mo</th><th>tu</th><th>we</th><th>th</th><th>fr</th><th>sa</th>       </tr></thead>       <tbody><tr><td><div time="1609089259774" data-tooltip="" class="day lastMonth  valid ">27</div></td><td><div time="1609175659774" data-tooltip="" class="day lastMonth  valid ">28</div></td><td><div time="1609262059774" data-tooltip="" class="day lastMonth  valid ">29</div></td><td><div time="1609348459774" data-tooltip="" class="day lastMonth  valid ">30</div></td><td><div time="1609434859774" data-tooltip="" class="day lastMonth  valid ">31</div></td><td><div time="1609521259774" data-tooltip="" class="day toMonth  valid ">1</div></td><td><div time="1609607659774" data-tooltip="" class="day toMonth  valid ">2</div></td></tr><tr><td><div time="1609694059774" data-tooltip="" class="day toMonth  valid real-today">3</div></td><td><div time="1609780459774" data-tooltip="" class="day toMonth  valid ">4</div></td><td><div time="1609866859774" data-tooltip="" class="day toMonth  valid ">5</div></td><td><div time="1609953259774" data-tooltip="" class="day toMonth  valid ">6</div></td><td><div time="1610039659774" data-tooltip="" class="day toMonth  valid ">7</div></td><td><div time="1610126059774" data-tooltip="" class="day toMonth  valid ">8</div></td><td><div time="1610212459774" data-tooltip="" class="day toMonth  valid ">9</div></td></tr><tr><td><div time="1610298859774" data-tooltip="" class="day toMonth  valid ">10</div></td><td><div time="1610385259774" data-tooltip="" class="day toMonth  valid ">11</div></td><td><div time="1610471659774" data-tooltip="" class="day toMonth  valid ">12</div></td><td><div time="1610558059774" data-tooltip="" class="day toMonth  valid ">13</div></td><td><div time="1610644459774" data-tooltip="" class="day toMonth  valid ">14</div></td><td><div time="1610730859774" data-tooltip="" class="day toMonth  valid ">15</div></td><td><div time="1610817259774" data-tooltip="" class="day toMonth  valid ">16</div></td></tr><tr><td><div time="1610903659774" data-tooltip="" class="day toMonth  valid ">17</div></td><td><div time="1610990059774" data-tooltip="" class="day toMonth  valid ">18</div></td><td><div time="1611076459774" data-tooltip="" class="day toMonth  valid ">19</div></td><td><div time="1611162859774" data-tooltip="" class="day toMonth  valid ">20</div></td><td><div time="1611249259774" data-tooltip="" class="day toMonth  valid ">21</div></td><td><div time="1611335659774" data-tooltip="" class="day toMonth  valid ">22</div></td><td><div time="1611422059774" data-tooltip="" class="day toMonth  valid ">23</div></td></tr><tr><td><div time="1611508459774" data-tooltip="" class="day toMonth  valid ">24</div></td><td><div time="1611594859774" data-tooltip="" class="day toMonth  valid ">25</div></td><td><div time="1611681259774" data-tooltip="" class="day toMonth  valid ">26</div></td><td><div time="1611767659774" data-tooltip="" class="day toMonth  valid ">27</div></td><td><div time="1611854059774" data-tooltip="" class="day toMonth  valid ">28</div></td><td><div time="1611940459774" data-tooltip="" class="day toMonth  valid ">29</div></td><td><div time="1612026859774" data-tooltip="" class="day toMonth  valid ">30</div></td></tr><tr><td><div time="1612113259774" data-tooltip="" class="day toMonth  valid ">31</div></td><td><div time="1612199659774" data-tooltip="" class="day nextMonth  valid ">1</div></td><td><div time="1612286059774" data-tooltip="" class="day nextMonth  valid ">2</div></td><td><div time="1612372459774" data-tooltip="" class="day nextMonth  valid ">3</div></td><td><div time="1612458859774" data-tooltip="" class="day nextMonth  valid ">4</div></td><td><div time="1612545259774" data-tooltip="" class="day nextMonth  valid ">5</div></td><td><div time="1612631659774" data-tooltip="" class="day nextMonth  valid ">6</div></td></tr></tbody>   </table><div class="dp-clearfix"></div><div class="time"><div class="time1"></div><div class="time2"></div></div><div class="dp-clearfix"></div></div><div class="footer"></div><div class="date-range-length-tip"></div></div></div>
					            </div>
					    	</div>
						</div>
							<div class="col-md-3">
					    		<div class="locationStart">
					    			<div class="Start">
					    				<i class="iconAngle fa fa-angle-down"></i>
					    				<i class="iconRight flaticon-bed"></i>
					    				<span>عدد الغرف و النزلاء</span>
					    				<span class="title">1 نزيل , 2ضيوف</span>
					    			</div>
					    			<div class="niceScroll contentToggle numberTravelers" tabindex="5004" style="overflow: hidden; outline: none;">
										<h2 class="title">عدد المسافرين <a href="#">اضف غرفة <i class="flaticon-plus-symbol"></i></a></h2>
										<ul class="numberContent">
											<li class="clearfix">
												<h3 class="text"><i class="flaticon-bald-man-with-moustache icon"></i> بالغ <span>( 12+ )</span></h3>
												<div class="numbers">
													<i class="fa fa-plus plus"></i>
													<span value="0" class="numb">0</span>
													<input type="hidden" class="inputHidden" value="0">
													<i class="fa fa-minus minus"></i>
												</div>
											</li>
											<li class="clearfix">
												<h3 class="text"><i class="flaticon-boy icon"></i> الأطفال <span>( 2-11 )</span></h3>
												<div class="numbers">
													<i class="fa fa-plus plus"></i>
													<span value="0" class="numb">0</span>
													<input type="hidden" class="inputHidden" value="0">
													<i class="fa fa-minus minus"></i>
												</div>
											</li>
											<li class="clearfix">
												<h3 class="text"><i class="flaticon2-baby-boy icon"></i> رضيع بدون مقعد <span>(   أقل من 2  )</span></h3>
												<div class="numbers">
													<i class="fa fa-plus plus"></i>
													<span value="0" class="numb">0</span>
													<input type="hidden" class="inputHidden" value="0">
													<i class="fa fa-minus minus"></i>
												</div>
											</li>
										</ul>
										<h2 class="title">تحديد الدرجة</h2>
					
										<ul class="checkList">
											<li class="clearfix">
												<label>
													اقتصادي                                     
													<input type="checkbox" class="check">
						                            <span></span>
						                        </label>
						                    </li>
											<li class="clearfix">
												<label>
													اقتصادي مميز                                     
													<input type="checkbox" class="check">
						                            <span></span>
						                        </label>
						                    </li>
											<li class="clearfix">
												<label>
													درجة أولى                                     
													<input type="checkbox" class="check">
						                            <span></span>
						                        </label>
						                    </li>
										</ul>
											
								
					    			</div>
					    		</div>
							</div>
							<div class="col-md-2">
								<a href="hotels.html" class="searchBtn"><i class="fa fa-search"></i> ابحث</a>
							</div>
						</div>
				
					</div>