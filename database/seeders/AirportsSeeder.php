<?php

namespace Database\Seeders;

use App\Models\Airport;
use App\Models\Dummy;
use App\Models\Country;

use Illuminate\Database\Seeder;

class AirportsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dummies = Dummy::all();

        foreach($dummies as $dummy){
            $country = Country::where('iso_code',$dummy->IsoCountry)->firstOrFail();

            Airport::create([
                'name' => [
                    'en' => $dummy->Title_en,
                    'ar' => $dummy->Title_ar
                ],
                'IATA_code' => $dummy->IATACode,
                'country_id' => $country->id,
                'municipality' => [
                    'en' => $dummy->Municipality_en,
                    'ar' => $dummy->Municipality_ar
                ]
            ]);
        }
    }
}
