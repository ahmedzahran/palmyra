<?php

namespace Database\Seeders;

use App\Models\Country;
use App\Models\Dummy;
use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $dummies = Dummy::all();

        foreach($dummies as $dummy){
            $country = Country::firstOrCreate(
                ['iso_code' => $dummy->IsoCountry],
                [
                'name' => [
                    'en' => $dummy->Country_en,
                    'ar' => $dummy->Country_ar
                ],
            ]);
        }
    }
}
