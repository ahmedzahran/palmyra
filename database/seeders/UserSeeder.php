<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Role::create([
            'name' => 'admin',
            'display_name' => 'User Administrator',
            'description' => 'User is allowed to manage and edit other users',
        ]);

        $member = Role::create([
            'name' => 'member',
            'display_name' => 'User member',
            'description' => 'member user ',
        ]);

        $admins = User::factory()
                    ->times(3)
                    ->create();

        $users = User::factory()
                    ->times(3)
                    ->create();

        $admins->each(function($user) use($admin){
            $user->attachRole($admin);
        });

        $users->each(function($user) use($member){
            $user->attachRole($member);
        });

    }
}
