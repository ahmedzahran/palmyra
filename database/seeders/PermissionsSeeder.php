<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Role::where('name','admin')->first();
        $adminUsers = User::whereRoleIs('admin')->get();
        $permissions = [];
        $permissions = [
            ['id' => 1, 'name' => 'user-access',],
            ['id' => 2, 'name' => 'user-create',],
            ['id' => 3, 'name' => 'user-edit',],
            ['id' => 4, 'name' => 'user-view',],
            ['id' => 5, 'name' => 'user-delete',],
            //country permissions
            ['id' => 6, 'name' => 'country-access'],
            ['id' => 7, 'name' => 'country-create',],
            ['id' => 8, 'name' => 'country-edit',],
            ['id' => 9, 'name' => 'country-view',],
            ['id' => 10, 'name' => 'country-delete',],
            //airport permissions
            ['id' => 11, 'name' => 'airport-access'],
            ['id' => 12, 'name' => 'airport-create',],
            ['id' => 13, 'name' => 'airport-edit',],
            ['id' => 14, 'name' => 'airport-view',],
            ['id' => 15, 'name' => 'airport-delete',],

            //currency permissions
            ['id' => 16, 'name' => 'currency-access'],
            ['id' => 17, 'name' => 'currency-create',],
            ['id' => 18, 'name' => 'currency-edit',],
            ['id' => 19, 'name' => 'currency-view',],
            ['id' => 20, 'name' => 'currency-delete',],

        ];

        foreach($permissions as $permission){
            $permission = Permission::create($permission);
            $admin->attachPermission($permission);
            array_push($permissions,$permission->id);
        }

        foreach($adminUsers as $admin){
            $admin->syncPermissions($permissions);
        }
    }
}
