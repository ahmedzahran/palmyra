$(document).ready(function () {

    $('[data-method]').append(function () {
        if (!$(this).find('form').length > 0) {
            return "\n<form action='" + $(this).attr('href') + "' method='POST' name='delete_item' style='display:none'>\n" +
                "<input type='hidden' name='_method' value='" + $(this).attr('data-method') + "'>\n" +
                "<input type='hidden' name='_token' value='" + $('meta[name="csrf-token"]').attr('content') + "'>\n" +
                '</form>\n';
        } else { return '' }
    }).attr('href', '#').attr('style', 'cursor:pointer;').attr('onclick', '$(this).find("form").submit();');


    $('body').on('submit', 'form[name=delete_item]', function (e) {
        e.preventDefault();

        const form = this;
        const link = $('a[data-method="delete"]');
        const cancel = (link.attr('data-trans-button-cancel')) ? link.attr('data-trans-button-cancel') : 'Cancel';
        const confirm = (link.attr('data-trans-button-confirm')) ? link.attr('data-trans-button-confirm') : 'Yes, delete';
        const title = (link.attr('data-trans-title')) ? link.attr('data-trans-title') : 'Are you sure you want to delete this item?';

        swal.fire({
            title: title,
            showCancelButton: true,
            confirmButtonText: confirm,
            cancelButtonText: cancel,
            type: 'warning'
        }).then((result) => {
            result.value && form.submit();
        });
    }).on('click', 'a[name=confirm_item]', function (e) {
        /**
         * Generic 'are you sure' confirm box
         */
        e.preventDefault();

        const link = $(this);
        const title = (link.attr('data-trans-title')) ? link.attr('data-trans-title') : 'Are you sure you want to do this?';
        const cancel = (link.attr('data-trans-button-cancel')) ? link.attr('data-trans-button-cancel') : 'Cancel';
        const confirm = (link.attr('data-trans-button-confirm')) ? link.attr('data-trans-button-confirm') : 'Continue';

        swal.fire({
            title: title,
            showCancelButton: true,
            confirmButtonText: confirm,
            cancelButtonText: cancel,
            type: 'info'
        }).then((result) => {
            result.value && window.location.assign(link.attr('href'));
        });
    });

    var handleCheckboxes = function (html, rowIndex, colIndex, cellNode) {
        var $cellNode = $(cellNode);
        var $check = $cellNode.find(':checked');
        return ($check.length) ? ($check.val() == 1 ? 'Yes' : 'No') : $cellNode.text();
    };

    var activeSub = $(document).find('.active-sub');
    if (activeSub.length > 0) {
        activeSub.parent().show();
        activeSub.parent().parent().find('.arrow').addClass('open');
        activeSub.parent().parent().addClass('open');
    }

    // $(document).on('click','.dataTable input[type=checkbox]',function () {
    //     $(this).parents('tr').toggleClass('selected');
    // })
    // window.dtDefaultOptions = {
    //     retrieve: true,
    //     dom: 'lfBrtip<"actions">',
    //     columnDefs: [],
    //     "iDisplayLength": 10,
    //     "aaSorting": [],
    //     buttons: [
    //         // {
    //         //     extend: 'copy',
    //         //     exportOptions: {
    //         //         columns: ':visible',
    //         //         format: {
    //         //             body: handleCheckboxes
    //         //         }
    //         //     }
    //         // },
    //         {
    //             extend: 'csv',
    //             exportOptions: {
    //                 columns: ':visible',
    //                 format: {
    //                     body: handleCheckboxes
    //                 }
    //             }
    //         },
    //         // {
    //         //     extend: 'excel',
    //         //     exportOptions: {
    //         //         columns: ':visible',
    //         //         format: {
    //         //             body: handleCheckboxes
    //         //         }
    //         //     }
    //         // },
    //         {
    //             extend: 'pdf',
    //             exportOptions: {
    //                 columns: ':visible',
    //                 format: {
    //                     body: handleCheckboxes
    //                 }
    //             }
    //         },
    //         // {
    //         //     extend: 'print',
    //         //     exportOptions: {
    //         //         columns: ':visible',
    //         //         format: {
    //         //             body: handleCheckboxes
    //         //         }
    //         //     }
    //         // },
    //         'colvis'
    //     ]
    // };
    // $('.datatable, .dataTable').each(function () {
    //     if ($(this).hasClass('dt-select')) {
    //         window.dtDefaultOptions.select = {
    //             style: 'multi',
    //             selector: 'td:first-child'
    //         };

    //         window.dtDefaultOptions.columnDefs.push({
    //             orderable: false,
    //             className: 'select-checkbox',
    //             targets: 0
    //         });
    //     }
    //     $(this).dataTable(window.dtDefaultOptions);
    // });

    if (typeof window.route_mass_crud_entries_destroy != 'undefined') {
        console.log($('.datatable, .ajaxTable, .dataTable').siblings('.actions'))
    }

    $(document).on('click', '.js-delete-selected', function (e) {
        if (confirm('Are you sure?')) {
            e.preventDefault();

            var ids = [];

            $(this).closest('.actions').siblings('.datatable,.dataTable, .ajaxTable').find('tbody tr.selected').each(function () {
                ids.push($(this).data('entry-id'));
            });

            $.ajax({
                method: 'POST',
                url: $(this).attr('href'),
                data: {
                    _token: _token,
                    ids: ids
                }
            }).done(function () {
                location.reload();
            });
        }

        return false;
    });

    $(document).on('click', '#select-all', function () {
        var selected = $(this).is(':checked');
        $(this).closest('table.datatable, table.dataTable, table.ajaxTable').find('td:first-child').each(function () {
            if (selected != $(this).closest('tr').hasClass('selected')) {
                $(this).click();
            }
        });
    });

    $('.mass').click(function () {
        if ($(this).is(":checked")) {
            $('.single').each(function () {
                if ($(this).is(":checked") == false) {
                    $(this).click();
                }
            });
        } else {
            $('.single').each(function () {
                if ($(this).is(":checked") == true) {
                    $(this).click();
                }
            });
        }
    });

    $('.page-sidebar').on('click', 'li > a', function (e) {

        if ($('body').hasClass('page-sidebar-closed') && $(this).parent('li').parent('.page-sidebar-menu').size() === 1) {
            return;
        }

        var hasSubMenu = $(this).next().hasClass('sub-menu');

        if ($(this).next().hasClass('sub-menu always-open')) {
            return;
        }

        var parent = $(this).parent().parent();
        var the = $(this);
        var menu = $('.page-sidebar-menu');
        var sub = $(this).next();

        var autoScroll = menu.data("auto-scroll");
        var slideSpeed = parseInt(menu.data("slide-speed"));
        var keepExpand = menu.data("keep-expanded");

        if (keepExpand !== true) {
            parent.children('li.open').children('a').children('.arrow').removeClass('open');
            parent.children('li.open').children('.sub-menu:not(.always-open)').slideUp(slideSpeed);
            parent.children('li.open').removeClass('open');
        }

        var slideOffeset = -200;

        if (sub.is(":visible")) {
            $('.arrow', $(this)).removeClass("open");
            $(this).parent().removeClass("open");
            sub.slideUp(slideSpeed, function () {
                if (autoScroll === true && $('body').hasClass('page-sidebar-closed') === false) {
                    if ($('body').hasClass('page-sidebar-fixed')) {
                        menu.slimScroll({
                            'scrollTo': (the.position()).top
                        });
                    }
                }
            });
        } else if (hasSubMenu) {
            $('.arrow', $(this)).addClass("open");
            $(this).parent().addClass("open");
            sub.slideDown(slideSpeed, function () {
                if (autoScroll === true && $('body').hasClass('page-sidebar-closed') === false) {
                    if ($('body').hasClass('page-sidebar-fixed')) {
                        menu.slimScroll({
                            'scrollTo': (the.position()).top
                        });
                    }
                }
            });
        }
        if (hasSubMenu == true || $(this).attr('href') == '#') {
            e.preventDefault();
        }
    });

    $('.select2').select2({
        placeholder: 'Choose..',
        allowClear: true
    });

    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        orientation: "bottom" 
    });

    $('#kt_reset').on('click',function(e){

        e.preventDefault();

        var table = $('.dataTable');

        $('.datatable-input').each(function() {
            $(this).val('');
            table.DataTable().column($(this).data('col-index')).search('', false, false);
        });

        $('.select2').each(function(){
            $(this).val('').trigger('change');
        });

        table.DataTable().ajax.reload();
    });


    $('#kt_search').on('click',function(e){
        e.preventDefault();
        var table = $('.dataTable');
        table.DataTable().ajax.reload();
    });
});

function jsValidateForm($formSelector,rules) {

    FormValidation.formValidation(document.getElementById($formSelector),{
        fields: rules,
        plugins: { //Learn more: https://formvalidation.io/guide/plugins
            trigger: new FormValidation.plugins.Trigger(),
            // Bootstrap Framework Integration
            bootstrap: new FormValidation.plugins.Bootstrap(),
            // Validate fields when clicking the Submit button
            submitButton: new FormValidation.plugins.SubmitButton(),
            // Submit the form when all fields are valid
            defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
        }
    });

    // $formSelector.validate({
    //     rules: rules
    // });
}
function selectSearch($url,$selector)
{
    $selector.select2({
        placeholder: "Search...",
        allowClear: true,
        minimumInputLength: 2,
        ajax: {
            url: $url,
            dataType: 'json',
            data: function (params) {
                return {
                    q: $.trim(params.term)
                };
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }
    })
}


function processAjaxTables() {
    $('.ajaxTable').each(function () {
        window.dtDefaultOptions.processing = true;
        window.dtDefaultOptions.serverSide = true;
        if ($(this).hasClass('dt-select')) {
            window.dtDefaultOptions.select = {
                style: 'multi',
                selector: 'td:first-child'
            };

            window.dtDefaultOptions.columnDefs.push({
                orderable: false,
                className: 'select-checkbox',
                targets: 0
            });
        }
        $(this).DataTable(window.dtDefaultOptions);
        if (typeof window.route_mass_crud_entries_destroy != 'undefined') {
            $(this).siblings('.actions').html('<a href="' + window.route_mass_crud_entries_destroy + '" class="btn btn-xs btn-danger js-delete-selected" style="margin-top:0.755em;margin-left: 20px;">Delete selected</a>');
        }
    });

}
