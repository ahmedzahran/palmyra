$(function(){
	
	
    $(".profileHeader .titleProfile").click(function () {
    	
    	$(this).siblings().toggleClass("active");
    });
        
	$('body,html').on('click', function(e) {
		var container = $(".profileHeader .titleProfile,.profileHeader .listProfile"),
		Sub = $(".profileHeader .listProfile");
		

	    if( !$(e.target).is(container) ){
	        Sub.removeClass("active");
	    }
	
	});
	
	$('[data-toggle="tooltip"]').tooltip();

	new WOW().init();
	/****** start nice scroll ******/
	niceScroll();
	function niceScroll() {
	
		$(".niceScroll").niceScroll({
		    cursorborderradius: 0,
			scrollspeed: 60,
			mousescrollstep: 38,
			cursorwidth: 6,
			cursorborder: 3,
			cursorcolor: 'rgba(228, 227, 228, 1)',
			zindex: 1500
			
	    });
	    
	}
	
	
	// Activating/deactivating scroll on modals:
	$('#searchModal').on('shown.bs.modal', function(e){	
		setTimeout(function() {
			$(".niceScroll22").niceScroll({
			    cursorborderradius: 0,
				scrollspeed: 60,
				mousescrollstep: 38,
				cursorwidth: 6,
				cursorborder: 3,
				cursorcolor: 'rgba(228, 227, 228, 1)',
				zindex: 1500
				
		    });
			
		});

	}).on('hide.bs.modal', function(e){	
	  $(".niceScroll22").niceScroll().remove();
	});
    
	/****** End nice scroll ******/
	
	$(window).scroll(function () {
		if($(this).scrollTop() > 200) {
			$(".header:not(.headerHotelDetails)").addClass("HeaderFixed");		
		} else {
			$(".header:not(.headerHotelDetails)").removeClass("HeaderFixed");
		}
	});
	
	$(window).bind('scroll', function() {
      var scrollTop = $(window).scrollTop();
      var elementOffset = $('.hotelDetails .tabsbtnsHead').offset().top;
      var currentElementOffset = (elementOffset - scrollTop);
      console.log(currentElementOffset);
      
      if(currentElementOffset <= 65) {
      	
      	$('.breadCrumb .searchContent').addClass("active");
      } else {
      	$('.breadCrumb .searchContent').removeClass("active");
      }
      
   });

	$(".openMenu").click(function () {
		$("body").addClass("overflowH");
		$(".menuMobile").fadeIn();
		$(".transformPage,.menuMobile .menuContent").addClass("active");
	});
	$(".menuMobile .closeX,.menuMobile .BgClose,.menuMobile .menuContent .menuRes li .link").click(function () {
		$("body").removeClass("overflowH");
		$(".menuMobile").fadeOut();
		$(".transformPage,.menuMobile .menuContent").removeClass("active");
	});
	
	$(".room .moreDetails").click(function () {
		$("body").addClass("overflowH");
		$(".SliderFixed").fadeIn(1000).addClass("active");
		$(".SliderFixed .sliderDetails").addClass("active");
	});
	$(".closeX,.BgClose").click(function () {
		$("body").removeClass("overflowH");
		$(".SliderFixed").fadeOut().removeClass("active");
		$(".SliderFixed .sliderDetails").removeClass("active");
	});
	

	
	$(".menuMobile .menuContent .menuRes li a").click(function (){
		$(this).siblings().slideToggle();
	});

	
	$(".headerToggle .titleToggle,.searchContent .locationStart .Start").click(function () {
		$(this).siblings().slideToggle();
		$(".contentToggle").not($(this).siblings()).slideUp();
	});
	
	$('body,html').on('click', function(e) {
		var container = $(".contentToggle .searchList *,.headerToggle .titleToggle *,.locationStart .Start,.locationStart .Start * ,.headerToggle .titleToggle,.contentToggle,.contentToggle *:not(.selectList *,.contentLocation .item *)"),
		Sub = $(".contentToggle");
		

	    if( !$(e.target).is(container) ){
	        Sub.slideUp();
	    }
	
	});
	
	contentToggle();
	function contentToggle(){

		$(".contentToggle .selectList li,.searchContent .contentLocation .item").click(function() {
					
			var myVal = $(this).attr("value");
			var myId = $(this).attr("idselect");
				$("#" + myId + " .spanVal").text(myVal);
				$("#" + myId + " .inputHidden").attr("value",myVal);
				
		}); 
	}
	
	$("#inputFilterPrice").on("keyup",function() {
		var value = $(this).val().toLowerCase();
		$("#selectFilterPrice li").filter(function () {
			$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
		});
	});
	
	$("#inputFilterPrice2").on("keyup",function() {
		var value = $(this).val().toLowerCase();
		$("#selectFilterPrice2 li").filter(function () {
			$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
		});
	});
	
	$("#inputFilterCountry").on("keyup",function() {
		var value = $(this).val().toLowerCase();
		$("#selectFilterCountry li").filter(function () {
			$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
		});
	});
	
	$("#inputFilterCountry2").on("keyup",function() {
		var value = $(this).val().toLowerCase();
		$("#selectFilterCountry2 li").filter(function () {
			$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
		});
	});
	
	
	$("#selectFilterCity").on("keyup",function() {
		var value = $(this).val().toLowerCase();
		$("#selectFilterCity li").filter(function () {
			$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
		});
	});
	
	$(".tabsbtns li").click(function () {
		
		var myButton = $(this).attr("id"),
			parent = $(this).parent().attr("id");
		$(this).addClass("active").siblings().removeClass("active");
		$("."+parent+" .tab").hide();
		$("."+parent+" ." + myButton).fadeIn();
		
	});
	
	$(".tabsbtns2 li").click(function () {
		
		var myButton = $(this).attr("id"),
			parent = $(this).parent().attr("id");
		$(this).addClass("active").siblings().removeClass("active");
		$("."+parent+" .tabsContent").hide();
		$("."+parent+" ." + myButton).fadeIn();
		
	});
	
	$(".listBtnsRight li").click(function () {
		
		var myButton = $(this).attr("id"),
			parent = $(this).parent().attr("id");
		$(this).addClass("active").siblings().removeClass("active");
		$("."+parent+" .tabPerson").hide();
		$("."+parent+" ." + myButton).fadeIn();
		
	});
	
	
	function plusDiv() {
	    $(".numberContent i.plus").click(function () {
	    	var x = $(this).next().text();
	    	x++;
	    	$(this).next().attr("value",x);
	    	$(this).next().text(x);
	    	$(this).next().next().val(x);
	    });
	    $(".numberContent i.minus").click(function () {
			var x = $(this).prev().attr("value");
			if(x > 0) {
				x--;
		    	$(this).prev().attr("value",x);
		    	$(this).prev().prev().text(x);
			}
	    });
   }
	
	if($("#openDateFirst")[0]) {
		 $('#openDateFirst').dateRangePicker({
			inline:true,
			container: '#date-range-container',
			alwaysOpen:true,
			singleMonth: true,
			singleDate : false,
			showTopbar: false
		});
	}
	if($("#openDateFirst2")[0]) {
		 $('#openDateFirst2').dateRangePicker({
			inline:true,
			container: '#date-range-container2',
			alwaysOpen:true,
			singleMonth: true,
			singleDate : false,
			showTopbar: false
		});
	}
	

	$(".locationStart .change").click(function(){
		
		$(this).toggleClass("active");
		
		var parent = $(this).parent().attr("id");
		
		var text1 = $(".tabsSearch .tab #"+ parent + " .titleToggle1").text();
		
		var text2 = $(".tabsSearch .tab #"+ parent + " .titleToggle2").text();
		
		$(".tabsSearch .tab #"+ parent + " .titleToggle1").text(text2);
		$(".tabsSearch .tab #"+ parent + " .titleToggle2").text(text1);

	});
	
	$(".searchContent .filtersTab2 li").click(function () {
		
		$(this).addClass("active").siblings().removeClass();
		
	});
	/*****************************************************************/
	
	$(".searchContent .addTrip").click(function () {
		
		var sId = Math.floor(Math.random()* 100);
		
		if($(".searchContent .tabsSearch .tab2 .row .row").length < 6) {
		
		$(".searchContent .tabsSearch .tab2 .row.parentRow").append(
			'<div class="row">'+
			'<div class="col-md-5">'+
			'<div class="locationStart clearfix selectLocation" id="selectLocation'+sId+'">'+
    			'<div class="float">'+
	    			'<div class="Start Start'+sId+'" id="selectStart'+sId+'">'+
	    				'<i class="iconAngle fa fa-angle-down"></i>'+
	    				'<i class="iconRight flaticon-target"></i>'+
	    				'<span>المغادرة من</span>'+
	    				'<span class="title spanVal spanVal titleToggle1">المدينة أو المطار</span>'+
	    			'</div>'+
	    			'<div class="contentToggle niceScroll contentLocation">'+
			    		'<h2 class="titleSection clearfix">عمليات بحثك الأخيرة <a href="#" class="remove">حذف</a></h2>'+
			    		'<div class="item" idselect="selectStart'+sId+'" value="مطار الملك خالدا لدولي">'+
			    			'<i class="icon flaticon-clock"></i>'+
			    			'<h2 class="title">مطار الملك خالدا لدولي</h2>'+
			    			'<span class="location">الرياض - المملكة العربية السعودية</span>'+
			    			'<span class="text">RUH</span>'+
			    		'</div>'+
			    		
			    		'<h2 class="titleSection clearfix">المغادرة من</h2>'+
			    		
			    		'<div class="item" idselect="selectStart'+sId+'" value="مطار الملك عبد العزيز الدولي">'+
			    			'<i class="icon flaticon-airplane"></i>'+
			    			'<h2 class="title">مطار الملك عبد العزيز الدولي</h2>'+
			    			'<span class="location">جدة - المملكة العربية السعودية</span>'+
			    			'<span class="text">CAI</span>'+
			    		'</div>'+
			    
	    			'</div>'+
    		
    			'</div>'+
    			'<a class="change flaticon-shuffle-arrow"></a>'+
    			'<div class="float">'+
	    			'<div class="Start Start'+sId+' end" id="selectEnd'+sId+'">'+
	    				'<i class="iconAngle fa fa-angle-down"></i>'+
	    				'<span>التوجهه إلى</span>'+
	    				'<span class="title spanVal spanVal titleToggle2">المدينة</span>'+
	    			'</div>'+
	    			'<div class="contentToggle niceScroll contentLocation">'+
			    		'<h2 class="titleSection clearfix">عمليات بحثك الأخيرة <a href="#" class="remove">حذف</a></h2>' +
			    		'<div class="item" idselect="selectEnd'+sId+'" value="مطار الملك خالدا لدولي">'+
			    			'<i class="icon flaticon-clock"></i>'+
			    			'<h2 class="title">مطار الملك خالدا لدولي</h2>'+
			    			'<span class="location">الرياض - المملكة العربية السعودية</span>'+
			    			'<span class="text">RUH</span>'+
			    		'</div>'+
			    		'<div class="item" idselect="selectEnd'+sId+'" value="مطار الملك عبد العزيز الدولي">'+
			    			'<i class="icon flaticon-clock"></i>'+
			    			'<h2 class="title">مطار الملك عبد العزيز الدولي</h2>'+
			    			'<span class="location">جدة - المملكة العربية السعودية</span>'+
			    			'<span class="text">CAI</span>'+
			    		'</div>'+
			    
			    		'<h2 class="titleSection clearfix">الانطلاق من</h2>'+
			    	
			    		'<div class="item" idselect="selectEnd'+sId+'" value="مطار الملك عبد العزيز الدولي">'+
			    			'<i class="icon flaticon-airplane"></i>'+
			    			'<h2 class="title">مطار الملك عبد العزيز الدولي</h2>'+
			    			'<span class="location">جدة - المملكة العربية السعودية</span>'+
			    			'<span class="text">CAI</span>'+
			    		'</div>'+
			    
	    			'</div>'+
    		
    			'</div>'+
			'</div>'+
    		'</div>'+
    		
			'<div class="col-md-3">'+
	    		'<div class="locationStart">'+
	    			'<div class="Start Start'+sId+'">'+
	    				'<i class="iconAngle fa fa-angle-down"></i>'+
	    				'<i class="iconRight flaticon-calendar"></i>'+
	    				'<span>ذهاب وعودة</span>'+
	    				'<span class="title">28 سبتمبر - 29 سبتمبر</span>'+
	    			'</div>'+
	    			'<div class="contentToggle contentTimer">'+
			  		'	<input type="text" id="openDateFirst'+sId+'" class="openDate" />'+
			  			'<div id="date-range-container'+sId+'"></div>'+
	    			'</div>'+
	    		'</div>'+
	    		'</div>'+
    		
					'<div class="col-md-2">'+
		    		'<div class="locationStart" >'+
		    			'<div class="Start Start'+sId+'">'+
		    				'<i class="iconAngle fa fa-angle-down"></i>'+
		    				'<i class="iconRight flaticon-bed"></i>'+
		    				'<span>عدد الغرف و النزلاء</span>'+
		    				'<span class="title">1 نزيل , 2ضيوف</span>'+
		    			'</div>'+
		    			'<div class="niceScroll contentToggle numberTravelers">'+
							'<h2 class="title">عدد المسافرين <a href="#">اضف غرفة <i class="flaticon-plus-symbol"></i></a></h2>'+
							'<ul class="numberContent">'+
							'	<li class="clearfix">'+
							'		<h3 class="text"><i class="flaticon-bald-man-with-moustache icon"></i> بالغ <span>( 12+ )</span></h3>'+
							'		<div class="numbers">'+
							'			<i class="fa fa-plus plus"></i>'+
							'			<span value="0" class="numb">0</span>'+
							'			<input type="hidden" class="inputHidden" value="0" />'+
							'			<i class="fa fa-minus minus"></i>'+
							'		</div>'+
							'	</li>'+
							'	<li class="clearfix">'+
							'		<h3 class="text"><i class="flaticon-boy icon"></i> الأطفال <span>( 2-11 )</span></h3>'+
							'		<div class="numbers">'+
							'			<i class="fa fa-plus plus"></i>'+
							'			<span value="0" class="numb">0</span>'+
							'			<input type="hidden" class="inputHidden" value="0" />'+
							'			<i class="fa fa-minus minus"></i>'+
							'		</div>'+
							'	</li>'+
							'	<li class="clearfix">'+
							'		<h3 class="text"><i class="flaticon2-baby-boy icon"></i> رضيع بدون مقعد <span>(   أقل من 2  )</span></h3>'+
							'		<div class="numbers">'+
							'			<i class="fa fa-plus plus"></i>'+
							'			<span value="0" class="numb">0</span>'+
							'			<input type="hidden" class="inputHidden" value="0" />'+
							'			<i class="fa fa-minus minus"></i>'+
							'		</div>'+
							'	</li>'+
							'</ul>'+
							'<h2 class="title">تحديد الدرجة</h2>'+
							'<ul class="checkList">'+
							'	<li class="clearfix">'+
							'		<label>'+
							'			اقتصادي                                 '+    
							'			<input type="checkbox" class="check" />'+
			                '           <span></span>'+
			                '       </label>'+
			                '    </li>'+
							'	<li class="clearfix">'+
							'		<label>'+
							'			اقتصادي مميز           '+                          
							'			<input type="checkbox" class="check" />'+
			                '           <span></span>'+
			                '       </label>'+
			                '    </li>'+
							'	<li class="clearfix">'+
							'		<label>'+
							'			درجة أولى                   '+                  
							'			<input type="checkbox" class="check" />'+
			                '           <span></span>'+
			                '       </label>'+
			                '    </li>'+
							'</ul>'+
		    			'</div>'+
	    		'</div>'+
	    		'</div>'+
			'<div class="col-md-2">'+
				'<i class="fa fa-trash removeDiv"></i>'+
			'</div>'+
	    	'</div>'	
    		).ready(function() {
    			
				console.log(".searchContent .locationStart .selectLocation"+sId+"");
				$(".searchContent .locationStart .Start"+sId+"").click(function () {
					$(this).siblings().slideToggle();
					$(".contentToggle").not($(this).siblings()).slideUp();
				});
				
				$("#selectLocation"+sId+" .change").click(function(){
					$(this).toggleClass("active");
					var parent = $(this).parent().attr("id");
					var text1 = $(".tabsSearch .tab #"+ parent + " .titleToggle1").text();
					var text2 = $(".tabsSearch .tab #"+ parent + " .titleToggle2").text();
					$(".tabsSearch .tab #"+ parent + " .titleToggle1").text(text2);
					$(".tabsSearch .tab #"+ parent + " .titleToggle2").text(text1);
				});

				contentToggle();
				
				if($("#openDateFirst"+sId+"")[0]) {
					 $("#openDateFirst"+sId+"").dateRangePicker({
						inline:true,
						container: "#date-range-container"+sId+"",
						alwaysOpen:true,
						singleMonth: true,
						singleDate : false,
						showTopbar: false
					});
				}
				
				plusDiv();
				niceScroll();
				
				$(".searchContent .removeDiv").click(function () {
					$(this).parent().parent().remove()
				});
				
				
				
    		});
    		
    	}

	});
	
	
	/******************************************************************/
	
	  var owlHotels = $('#owlHotels');
	 
	  owlHotels.owlCarousel({
	      
	      items : 5, //10 items above 1000px browser width
	      itemsDesktop : [1200,5], //5 items between 1000px and 901px
	      itemsDesktopSmall : [979,3], // betweem 900px and 601px
	      itemsTablet: [768,2], //2 items between 600 and 0
	      itemsMobile : [479,1],// itemsMobile disabled - inherit from itemsTablet option
	      slideSpeed : 500,
	      paginationSpeed : 400,
	      pagination:false,
	      navigation:false,
	      autoPlay:true,
	      navigationText:["<i class='fa fa-angle-left fa-2x'></i>","<i class='fa fa-angle-right fa-2x'></i>"]
	  });
	  
	
	$(".heart").click(function () {
		$(this).toggleClass("active")
	})
	
	$(".openShare").click(function () {
		$(this).siblings().slideToggle()
	})
	  
	$('body,html').on('click', function(e) {
		var container = $(".openShare,.listSocial,.openShare i"),
		Sub = $(".listSocial");
		

	    if( !$(e.target).is(container)  ){
	        Sub.slideUp();
	    }
	

	});
	
	// $('.rate').raty({
	// 	score:2,
	// 	starOff: 'images/star-off.png',
	// 	starOn: 'images/star-on.png',
	// 	number:7
	// });
	
	
	$(".formLogin .inputStyle input").keyup(function() {
	    if($(this).val().length > 0) {
	         $(this).parent().addClass("active");
	    } else {
	        $(this).parent().removeClass("active");
	    }
	});

	
	
	$(".openLoginBtn").click(function () {
		$(".openLogin").addClass("activeFormLogin");
	});
	$(".openSingUpBtn").click(function () {
		$(".openSingUp").addClass("activeFormSingUp");
	});
	$(".openForgetBtn").click(function () {
		$(".openForget").addClass("activeFormForget");
	});
	
	$(".closeFormLogin").click(function () {
		$(this).parent().removeClass("activeFormLogin");
	});
	$(".closeFormSingUp").click(function () {
		$(this).parent().removeClass("activeFormSingUp");
	});
	$(".closeFormForget").click(function () {
		$(this).parent().removeClass("activeFormForget");
	});

	  var owlAbout = $('#owlAbout');
	 
	  owlAbout.owlCarousel({
	      
	      items : 1, //10 items above 1000px browser width
	      itemsDesktop : [1200,1], //5 items between 1000px and 901px
	      itemsDesktopSmall : [979,1], // betweem 900px and 601px
	      itemsTablet: [768,1], //2 items between 600 and 0
	      itemsMobile : [479,1],// itemsMobile disabled - inherit from itemsTablet option
	      slideSpeed : 500,
	      paginationSpeed : 400,
	      pagination:false,
	      navigation:true,
	      autoPlay:true,
	      navigationText:["<i class='fa fa-angle-left fa-2x'></i>","<i class='fa fa-angle-right fa-2x'></i>"]
	  });
	

	/****** Start accordion ******/
	
	$(".accordion.active .accordion-content").css("display","block");
	
	$(".accordion .accordion-title").click(function () {
		
		var accordId = $(this).parent().parent().attr("id");
				
		$(this).next().slideToggle(500);
		
		/*$("#"+accordId + " .accordion .accordion-content").not($(this).next()).slideUp(500);
		*/
		$(this).parent().toggleClass("active");
		
				
	});
	

  	$(".formBookNow .selectStyle .titleSelect,.formBookNow .selectStyle .icon").click(function() {
  		
  		$(this).parent().toggleClass("active");
  		
  		var idParent = $(this).parent().attr("id");
  		$(".formBookNow .selectStyle .selectList").not($("."+idParent)).slideUp();
  		
  		
  		$("."+idParent).slideToggle();
  		
  	});
  	
	$('body,html').on('click', function(e) {
		var container = $(".formBookNow .selectStyle .titleSelect,.formBookNow .selectStyle .selectList,.formBookNow .selectStyle .icon"),
		Sub = $(".formBookNow .selectList");
		

	    if( !$(e.target).is(container)  ){
	    	Sub.parent().removeClass("active");
	        Sub.slideUp();
	    }
	

	});
  	
	$(".formBookNow .selectStyle .selectList li span").click(function() {
		
		
		
		$(this).parent().toggleClass("active").siblings().removeClass("active");
		
		var myVal = $(this).attr("value");
		var myId = $(this).attr("idselect");
		var mySrc = $(this).attr("imageSrc");
				
		if ($(this).parent().hasClass("active")) {
		    
			$("#" + myId + " .titleSelect").text(myVal);
			
			$("#" + myId + " .inputHidden").attr("value",myVal);
			
			$("#" + myId + " .imgCountry").attr("src",mySrc);
			
		    
		} else {
			
			if($("#" + myId).hasClass("selectStyleCheck")){
				var myValText = $("#" + myId + " .titleSelect").attr("value");
				$("#" + myId + " .titleSelect").text(myValText);
				
				$("#" + myId + " .inputHidden").attr("value","");
				
				$("#" + myId + " .imgCountry").attr("src","images/saudi-arabia (1).png");
			}
			
		}
		
	}); 
        

	if($(".phone")[0]) {
		  var input = document.querySelector(".phone");
		    window.intlTelInput(input, {
		       //allowDropdown: false,
		       //autoHideDialCode: false,
		       //autoPlaceholder: "off",
		       //dropdownContainer: document.body,
		      // excludeCountries: ["sn"],
		      // formatOnDisplay: false,
		      // geoIpLookup: function(callback) {
		      //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
		      //     var countryCode = (resp && resp.country) ? resp.country : "";
		      //     callback(countryCode);
		      //   });
		      // },
		      // hiddenInput: "full_number",
		      // initialCountry: "auto",
		      // localizedCountries: { 'de': 'Deutschland' },
		      // nationalMode: false,
		      // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
		       //placeholderNumberType: "none",
		       preferredCountries: ['sa'],
		      // separateDialCode: true,
		      //utilsScript: "js/utils.js",
		    });
		    
		   
	}
        
     $( ".datepicker" ).datepicker();

	$(".detailsVisa .visaImages .col-xs-4").click(function () {
		$(this).addClass("active").siblings().removeClass("active");
	});
	
	$(".specialOrders .textareaStyle textarea").keyup(function() {
	    if($(this).val().length > 0) {
	         $(this).siblings().css("display","none");
	    } else {
	        $(this).siblings().css("display","block");
	    }
	});
	
	$(".ticket .select").click(function () {
		$(this).toggleClass("active");
		$(".ticket .select").not($(this)).removeClass("active");
		
		
	});
	
	$(".tickets .headTitles.toggleHead .toggleContent .toggleAngle").click(function () {
		
		$(this).siblings().slideToggle();
		
	});
	
	$('body,html').on('click', function(e) {
		var container = $(".toggleContent .toggleAngle,.toggleContent .listToggle"),
		Sub = $(".toggleContent .listToggle");
		

	    if( !$(e.target).is(container) ){
	        Sub.slideUp();
	    }
	
	
	});
	
	

		/*
		$(".childAgeProgress").rangeSlider({
	        bounds: { max: 500,min: 1},
	        arrows: false,
	        defaultValues: {min: $("#MinPrice").text(), max: $("#MaxPrice").text()},
	        symmetricPositionning: false,
	        range: {min: 0}
		});
	    $(".childAgeProgress").bind("valuesChanging", function (e, data) {
	        $("#MinPrice").text(parseInt(data.values.min));
	        $("#MaxPrice").text(parseInt(data.values.max));
	
	        });*/

	$("#PriceSlider").rangeSlider({
        bounds: { max: 10000,min: 500},
        arrows: false,
        defaultValues: {min: $("#MinPrice").text(), max: $("#MaxPrice").text()},
        symmetricPositionning: false,
        range: {min: 3000}
    });
    $("#PriceSlider").bind("valuesChanging", function (e, data) {
        $("#MinPrice").text(parseInt(data.values.min));
        $("#MaxPrice").text(parseInt(data.values.max));

        });
        
	  var OwlSliderAbout = $('#OwlSliderAbout,#OwlSliderAbout2');
	 
	  OwlSliderAbout.owlCarousel({
	      items : 1, //10 items above 1000px browser width
	      itemsDesktop : [1200,1], //5 items between 1000px and 901px
	      itemsDesktopSmall : [979,1], // betweem 900px and 601px
	      itemsTablet: [768,1], //2 items between 600 and 0
	      itemsMobile : [479,1],// itemsMobile disabled - inherit from itemsTablet option
	      slideSpeed : 500,
	      paginationSpeed : 400,
	      pagination:true,
	      navigation:false,
	      autoPlay:true,
	      navigationText:["<i class='fa fa-angle-left fa-2x'></i>","<i class='fa fa-angle-right fa-2x'></i>"]
	  });
	
	$(".breadCrumb .edit").click(function() {
		
		$(".breadCrumb .searchContent").toggleClass("activeAnim");
		
	});
	
	
	  var OwlMoreHotels = $('#OwlMoreHotels');
	 
	  OwlMoreHotels.owlCarousel({
	      items : 4, //10 items above 1000px browser width
	      itemsDesktop : [1200,4], //5 items between 1000px and 901px
	      itemsDesktopSmall : [979,3], // betweem 900px and 601px
	      itemsTablet: [768,2], //2 items between 600 and 0
	      itemsMobile : [479,1],// itemsMobile disabled - inherit from itemsTablet option
	      slideSpeed : 500,
	      paginationSpeed : 400,
	      pagination:false,
	      navigation:true,
	      autoPlay:true,
	      navigationText:["<i class='fa fa-angle-left fa-2x'></i>","<i class='fa fa-angle-right fa-2x'></i>"]
	  });
	  
	  var carouselIndicators = $('#carousel-indicators');
	 
	  carouselIndicators.owlCarousel({
	      items : 5, //10 items above 1000px browser width
	      itemsDesktop : [1200,4], //5 items between 1000px and 901px
	      itemsDesktopSmall : [979,4], // betweem 900px and 601px
	      itemsTablet: [768,2], //2 items between 600 and 0
	      itemsMobile : [479,2],// itemsMobile disabled - inherit from itemsTablet option
	      slideSpeed : 500,
	      paginationSpeed : 400,
	      pagination:false,
	      navigation:false,
	      autoPlay:true,
	      navigationText:["<i class='fa fa-angle-left fa-2x'></i>","<i class='fa fa-angle-right fa-2x'></i>"]
	  });
        
    $(".reviewsHead .rightSection .langs .titleLangs").click(function () {
    	
    	$(this).siblings().slideToggle();
    });
        
	$('body,html').on('click', function(e) {
		var container = $(".reviewsHead .rightSection .langs .titleLangs, .rightSection .langs .langBtns"),
		Sub = $(".rightSection .langs .langBtns");
		

	    if( !$(e.target).is(container) ){
	        Sub.slideUp();
	    }
	
	});

	
	
});

	function myFunction(nameId) {
	  var x = document.getElementById(nameId);
	  if (x.type === "password") {
	    x.type = "text";
	  } else {
	    x.type = "password";
	  }
	}


