<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => '/','namespace' => 'Website'],function(){

    // Route::get('/', function () {
    //     return view('welcome');
    // });
    Route::get('/','HomeController@index');

});

Route::group(['prefix' => 'dashboard', 'namespace' => 'Dashboard','middleware' => ['role:admin']], function () {
    Route::get('/', 'DashboardController@index')->name('admin.dashboard');

    Route::group(['prefix' => 'users'],function(){

        Route::get('/','UsersController@index')
            ->name('admin.users.index')
            ->middleware('permission:user-access');

        Route::get('/create','UsersController@create')
            ->name('admin.users.create')
            ->middleware('permission:user-create');

        Route::delete('/delete/{user_id}','UsersController@delete')
            ->name('admin.users.delete')
            ->middleware('permission:user-delete');

        Route::get('/edit/{user_id}','UsersController@edit')
            ->name('admin.users.edit')
            ->middleware('permission:user-edit');

        Route::post('/update/{user_id}','UsersController@update')
            ->name('admin.users.update')
            ->middleware('permission:user-edit');

        Route::post('/','UsersController@store')
            ->name('admin.users.store')
            ->middleware('permission:user-create');
    });

    Route::group(['prefix' => 'countries'],function(){

        Route::get('/','CountriesController@index')
            ->name('admin.countries.index')
            ->middleware('permission:country-access');

        Route::get('/create','CountriesController@create')
            ->name('admin.countries.create')
            ->middleware('permission:country-create');

        Route::delete('/delete/{country_id}','CountriesController@delete')
            ->name('admin.countries.delete')
            ->middleware('permission:country-delete');

        Route::get('/edit/{country_id}','CountriesController@edit')
            ->name('admin.countries.edit')
            ->middleware('permission:country-edit');

        Route::post('/update/{country_id}','CountriesController@update')
            ->name('admin.countries.update')
            ->middleware('permission:country-edit');

        Route::post('/','CountriesController@store')
            ->name('admin.countries.store')
            ->middleware('permission:country-create');

        Route::get('find','CountriesController@find')->name('admin.countries.find')
            ->middleware('permission:country-create');
    });


    Route::group(['prefix' => 'airports'],function(){

        Route::get('/','AirportsController@index')
            ->name('admin.airports.index')
            ->middleware('permission:airport-access');

        Route::get('/create','AirportsController@create')
            ->name('admin.airports.create')
            ->middleware('permission:airport-create');

        Route::delete('/delete/{airport_id}','AirportsController@delete')
            ->name('admin.airports.delete')
            ->middleware('permission:airport-delete');

        Route::get('/edit/{airport_id}','AirportsController@edit')
            ->name('admin.airports.edit')
            ->middleware('permission:airport-edit');

        Route::post('/update/{airport_id}','AirportsController@update')
            ->name('admin.airports.update')
            ->middleware('permission:airport-edit');

        Route::post('/','AirportsController@store')
            ->name('admin.airports.store')
            ->middleware('permission:airport-create');
    });


    Route::group(['prefix' => 'currencies'],function(){

        Route::get('/','CurrenciesController@index')
            ->name('admin.currencies.index')
            ->middleware('permission:currency-access');

        Route::get('/create','CurrenciesController@create')
            ->name('admin.currencies.create')
            ->middleware('permission:currency-create');

        Route::delete('/delete/{currency_id}','CurrenciesController@delete')
            ->name('admin.currencies.delete')
            ->middleware('permission:currency-delete');

        Route::get('/edit/{currency_id}','CurrenciesController@edit')
            ->name('admin.currencies.edit')
            ->middleware('permission:currency-edit');

        Route::post('/update/{currency_id}','CurrenciesController@update')
            ->name('admin.currencies.update')
            ->middleware('permission:currency-edit');

        Route::post('/','CurrenciesController@store')
            ->name('admin.currencies.store')
            ->middleware('permission:currency-create');
    });

    Route::get('/lang/{lang}', 'DashboardController@cahngeLang')->name('admin.changeLang');
});

Auth::routes();

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('auth/{provider}', [App\Http\Controllers\SocialiteController::class, 'redirect']);

Route::get('auth/{provider}/callback',[App\Http\Controllers\SocialiteController::class, 'Callback']);